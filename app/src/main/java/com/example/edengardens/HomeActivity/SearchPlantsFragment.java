package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Plant.PlantCardAdapter;
import com.example.edengardens.RecyclerView.Plant.PlantCardItem;
import com.example.edengardens.RecyclerView.Plant.PlantCardItemSINGLETON;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Objects;

public class  SearchPlantsFragment extends Fragment implements PlantCardAdapter.OnItemListener{

    private static final String LOG = "Search-Plant-Fragment_LAB";

    private PlantCardAdapter adapter;

    private static final String TAG = "PLANTS_REQUEST";
    private Connection connection;

    private ConstraintLayout progressBarLayout;

    private View view;

    private SharedPreferences sharedPreferences;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.search_plants_fragment, container, false);

        activity = getActivity();
        if(activity != null) {

            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);

            sharedPreferences = activity.getSharedPreferences(Utilities.KEY_CATEGORIA, Activity.MODE_PRIVATE);

            SearchView searchView = view.findViewById(R.id.idSearchPianta);

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    return true;
                }
            });
            //riscrivo la netwrokcallback della connection in modo che ogni volta che sente internet ricarica la pagina
            connection.setNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    connection.setNetworkConnected(true);
                    connection.dismissSnackbar();
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() { //questo metodo serve per far girare il codice sul thread principale, perchè solo li posso cambaire la view
                            createRequest();
                        }
                    });

                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    connection.setNetworkConnected(false);
                    connection.showSnackbar();
                }

            });


            progressBarLayout = view.findViewById(R.id.progressBarPiantaLayout);

            connection.showSnackbar(); //faccio vidualizzare di default la snackbar che non c'è connessione

        }
        return view;

    }

    private void setRecyclerView(View view){
        // Set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.idRecyclerViewPianta);

        adapter = new PlantCardAdapter(PlantCardItemSINGLETON.getInstance().getListPianta(), this);
        recyclerView.setAdapter(adapter);
    }

    private void createRequest(){
        int idCategoria = sharedPreferences.getInt(Utilities.KEY_CATEGORIA, 1);
        String url = "https://ftp.edengardens.altervista.org/piante.php?categoria="+idCategoria ;

        progressBarLayout.setVisibility(View.VISIBLE);

        PlantCardItemSINGLETON.getInstance().emptyList();

        //creiamo richiesta di tipo json con un array contenente almeno 1 elemento
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                try {
                    for(int i=0;i<response.length();i++){
                        int id=response.getJSONObject(i).getInt("idPianta");
                        String nome=response.getJSONObject(i).getString("nome");
                        String descrizione = response.getJSONObject(i).getString("descrizione");
                        String immagine=response.getJSONObject(i).getString("immaginePianta");
                        PlantCardItem plantCardItem =new PlantCardItem(id,nome, descrizione, immagine);
                        PlantCardItemSINGLETON.getInstance().addCardItemPianta(plantCardItem);
                    }

                    setRecyclerView(view);

                    progressBarLayout.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    //c'è stato qualche problema
                    progressBarLayout.setVisibility(View.GONE);
                    AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                            .setTitle(R.string.errore)
                            .setMessage(R.string.problema)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            }).create();
                    alert.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                Log.e(TAG, error.toString());
            }
        });

        //bisogna assegnarli un tag e inserirlo nella coda
        jsonArrayRequest.setTag(TAG);
        //requestQueue.add(jsonArrayRequest);
        connection.addRequest(jsonArrayRequest);

    }

    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo

        connection.close();
    }

    @Override
    public void onItemClick(int position) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_PIANTA_SELEZIONATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
        Gson gson = new Gson();
        String json = gson.toJson(PlantCardItemSINGLETON.getInstance().getCardPiantaItem(position));
        editor.putString(Utilities.KEY_PIANTA_SELEZIONATA,json);
        editor.apply(); //NECESSARIO

        HomeActivity activity = (HomeActivity)getActivity();
        if(activity != null)
            activity.addIdInBackList(activity.getLastItemInBackList());

        Utilities.insertFragment(activity, new ShowPlantFragment(), "ShowPlantFragment", R.id.fragment_container_home,true);
    }
}
