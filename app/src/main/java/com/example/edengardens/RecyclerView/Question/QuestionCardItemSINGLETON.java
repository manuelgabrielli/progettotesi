package com.example.edengardens.RecyclerView.Question;

import java.util.ArrayList;
import java.util.List;

public class QuestionCardItemSINGLETON {
    private List<QuestionCardItem> questionCardItemList =new ArrayList<>();
    private List<QuestionCardItem> filteredQuestionCardItemList =new ArrayList<>();
    private static final QuestionCardItemSINGLETON ourInstance = new QuestionCardItemSINGLETON();
    private List<QuestionCardItem> followList = new ArrayList<>();
    private List<QuestionCardItem> plantQuestions =new ArrayList<>();

    public static QuestionCardItemSINGLETON getInstance(){
        return ourInstance;
    }

    private QuestionCardItemSINGLETON(){
    }

    public void setFilteredQuestionCardItemList(List<QuestionCardItem> list){
        this.filteredQuestionCardItemList = new ArrayList<>(list);
    }

    public void setFollowCardItemList(List<QuestionCardItem> list){
        this.followList = new ArrayList<>(list);
    }

    public void setPlantQuestionsCardItemList(List<QuestionCardItem> list){
        this.plantQuestions = new ArrayList<>(list);
    }

    public boolean isQuestionListFiltered(){
        return !this.filteredQuestionCardItemList.isEmpty();
    }

    public QuestionCardItem getFilteredCardDomandaItem(int position){
        return filteredQuestionCardItemList.get(position);
    }

    public void addCardItemDomandaOnTop(QuestionCardItem questionCardItem){
        this.questionCardItemList.add(0, questionCardItem);
    }
    public void addFollowCard(QuestionCardItem questionCardItem){
        this.followList.add(0, questionCardItem);
    }
    public void addPlantQuestion(QuestionCardItem questionCardItem){
        this.plantQuestions.add(0, questionCardItem);
    }


    public void addCardItemDomanda(QuestionCardItem questionCardItem){
        this.questionCardItemList.add(questionCardItem);
    }

    public List<QuestionCardItem> getFollowList(){return this.followList;}

    public List<QuestionCardItem> getListDomanda(){
        return questionCardItemList;
    }

    public List<QuestionCardItem> getListPlantQuestions(){
        return plantQuestions;
    }

    public QuestionCardItem getCardDomandaItem(int position){
        return questionCardItemList.get(position);
    }

    public QuestionCardItem getFollowCard(int position){
        return this.followList.get(position);
    }

    public QuestionCardItem getPlantQuestion(int position){
        return this.plantQuestions.get(position);
    }

    public void emptyFollowList(){this.followList.clear();}

    public void emptyList(){
        questionCardItemList.clear();
    }

    public void emptyPlantQuestions(){
        plantQuestions.clear();
    }

    public QuestionCardItem getDomandaInList(int idDomanda){
        for(int i = 0; i< questionCardItemList.size(); i++){
            if(questionCardItemList.get(i).getId() == idDomanda){
                return getCardDomandaItem(i);
            }
        }
        return getCardDomandaItem(0);
    }
}
