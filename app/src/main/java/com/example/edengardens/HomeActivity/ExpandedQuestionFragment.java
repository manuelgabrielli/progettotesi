package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;

import com.example.edengardens.RecyclerView.Question.QuestionCardItem;
import com.example.edengardens.RecyclerView.Answer.AnswerCardAdapter;
import com.example.edengardens.RecyclerView.Answer.AnswerCardItem;
import com.example.edengardens.RecyclerView.Answer.AnswerCardItemSINGLETON;
import com.example.edengardens.Utilities;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;


public class ExpandedQuestionFragment extends Fragment  implements AnswerCardAdapter.OnItemListener{

    private SharedPreferences sharedPreferences;
    private SwitchMaterial switchMaterial;

    private  Boolean seguito=false;//mi salvo se come impostare lo switches

    private RecyclerView recyclerView;
    private AnswerCardAdapter adapter;
    private View view;

    private static final String LOG = "ExspansiveQuestion_LAB";
    private static final String TAG = "EXSPANSIVE_REQUEST";
    private Connection connection;
    private QuestionCardItem domandaItem;
    private boolean check = false;
    private ConstraintLayout noRisposteLayout;
    private ConstraintLayout progressBarLayout;
    private String emailAutore;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.expanded_question_layout, container, false);

        activity = getActivity();
        if(activity != null) {
            sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, Context.MODE_PRIVATE);
            emailAutore = sharedPreferences.getString(Utilities.KEY_USERNAME, "");

            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);
            connection.setNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    connection.setNetworkConnected(true);
                    connection.dismissSnackbar();
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() { //questo metodo serve per far girare il codice sul thread principale, perchè solo li posso cambaire la view
                            createRequestInitial(view);
                            createRequest(view);
                        }
                    });

                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    connection.setNetworkConnected(false);
                    connection.showSnackbar();
                }

            });

            sharedPreferences = activity.getSharedPreferences(Utilities.KEY_DOMANDASELEZIONATA, Activity.MODE_PRIVATE);
            Gson gson = new Gson();
            String json;
            String categoria = sharedPreferences.getString(Utilities.CATEGORIA_DOMANDA, "");
            //faccio lo switch per predere la domanda giusta in base da che fragment si arriva
            switch (categoria) {
                case Utilities.KEY_DOMANDASELEZIONATA_HOME:
                    json = sharedPreferences.getString(Utilities.KEY_DOMANDASELEZIONATA_HOME, "");
                    break;
                case Utilities.KEY_DOMANDASELEZIONATA_FOLLOW:
                    json = sharedPreferences.getString(Utilities.KEY_DOMANDASELEZIONATA_FOLLOW, "");
                    break;
                case Utilities.KEY_DOMANDASELEZIONATA_SEARCH:
                    json = sharedPreferences.getString(Utilities.KEY_DOMANDASELEZIONATA_SEARCH, "");
                    break;
                default:
                    json = "";
            }

            domandaItem = gson.fromJson(json, QuestionCardItem.class);

            final TextView nomeProfilo = view.findViewById(R.id.idNomeProfiloEx);
            TextView data = view.findViewById(R.id.idDataEx);
            TextView titolo = view.findViewById(R.id.idTitoloEx);
            TextView domanda = view.findViewById(R.id.idDomandaEx);
            noRisposteLayout = view.findViewById(R.id.noRisposte1Layout);
            progressBarLayout = view.findViewById(R.id.progressBarRisposta1Layout);


            switchMaterial = view.findViewById(R.id.idSwitchEx);
            ImageView imageView = view.findViewById(R.id.idImmagineEx);
            recyclerView = view.findViewById(R.id.idRecyclerEx);
            Button button = view.findViewById(R.id.idAddRisponti);
            if (domandaItem.getAutore().equals(emailAutore)) {
                switchMaterial.setVisibility(View.GONE);
            } else{
                //inserisco il click sul profilo dell'autore della domanda
                nomeProfilo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences sp =  activity.getSharedPreferences(Utilities.KEY_OTHER_USER_PROFILE, Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit(); //serve per aggiungere dati allo sP
                        editor.putString(Utilities.KEY_OTHER_USER_PROFILE, domandaItem.getAutore());
                        editor.apply(); //NECESSARIO
                        HomeActivity act = (HomeActivity)activity;
                        act.addIdInBackList(act.getLastItemInBackList());
                        Utilities.insertFragment((FragmentActivity) activity, new ProfileOtherUserFragment(), "ProfileOtherUserFragment", R.id.fragment_container_home, true);
                    }
                });
            }
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity act = (HomeActivity)activity;
                    act.addIdInBackList(act.getLastItemInBackList());
                    Utilities.insertFragment((FragmentActivity) activity, new AddAnswerFragment(), "HomeFragment", R.id.fragment_container_home, true);
                }
            });


            switchMaterial.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    //metto il controllo in modo che la prima volta che setta lo switch non richiami il metodo
                    if (check) {
                        createRequest2(view);
                    }
                }
            });


            if (domandaItem.isimagePresent()) {
               Picasso.get()
                        .load(Utilities.IMAGE_PREFIX + domandaItem.getImage())
                        .resize(400, 400)
                        .onlyScaleDown()
                        .centerCrop()
                        .into(imageView);
            } else {
                ImageView backgroundImage = view.findViewById(R.id.idBackgroundImage);
                backgroundImage.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
            }


            nomeProfilo.setText(domandaItem.getAutore());
            data.setText(Utilities.changeDateFormat(domandaItem.getDate()));
            titolo.setText(domandaItem.getTitolo());
            domanda.setText(domandaItem.getDomanda());
        }
        return view;
    }


    private void setRecyclerView(View view){
        // Set up the RecyclerView
        recyclerView = view.findViewById(R.id.idRecyclerEx);
        //recyclerView.setHasFixedSize(true);
        adapter = new AnswerCardAdapter(AnswerCardItemSINGLETON.getInstance().getListRisposta(), this);
        recyclerView.setAdapter(adapter);

        if(AnswerCardItemSINGLETON.getInstance().getListRisposta().isEmpty()){
            //faccio vedere il layout della scritta
            noRisposteLayout.setVisibility(View.VISIBLE);
        } else {
            noRisposteLayout.setVisibility(View.GONE);
        }
    }

    private void createRequest(final View view){

        String url = "https://ftp.edengardens.altervista.org/risposta.php?idDomanda="+ domandaItem.getId();

        //svuoto la lista
        AnswerCardItemSINGLETON.getInstance().emptyListRisposta();

        progressBarLayout.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++){

                                int id=jsonArray.getJSONObject(i).getInt("idRisposta");
                                String testo=jsonArray.getJSONObject(i).getString("testoRisposta");
                                String date=jsonArray.getJSONObject(i).getString("dataRisposta");
                                String autore=jsonArray.getJSONObject(i).getString("email");
                                double valutazione;
                                if(jsonArray.getJSONObject(i).getDouble("numeroValutazioni")==0)
                                     valutazione=0;
                                else
                                     valutazione=jsonArray.getJSONObject(i).getDouble("punteggioTotale")/jsonArray.getJSONObject(i).getDouble("numeroValutazioni");
                                AnswerCardItem answerCardItem =new AnswerCardItem(id,autore,testo,date,(float)valutazione);
                                AnswerCardItemSINGLETON.getInstance().addCardItemRisposta(answerCardItem);

                            }

                            //progressBarLayout.setVisibility(View.GONE);

                            //ordino la lista
                            Collections.sort(AnswerCardItemSINGLETON.getInstance().getListRisposta(), new Comparator<AnswerCardItem>() {
                                        @Override
                                        public int compare(AnswerCardItem o1, AnswerCardItem o2) {
                                            return Float.compare(o2.getRaking(), o1.getRaking());
                                        }
                            });

                            progressBarLayout.setVisibility(View.GONE);

                            setRecyclerView(view);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){

        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);
        connection.addRequest(stringRequest);
    }

    private void createRequest2(final View view){

        String url;
        if(!seguito)
             url = "https://ftp.edengardens.altervista.org/addFollow.php?email="+emailAutore+"&idDomanda="+ domandaItem.getId();
        else
            url="https://ftp.edengardens.altervista.org/deleteFollow.php?email="+emailAutore+"&idDomanda="+ domandaItem.getId();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        seguito=!seguito;
                        setRecyclerView(view);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){

        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }

    //controlla se è seguito o no
    private void createRequestInitial(final View view){

        String url ="https://ftp.edengardens.altervista.org/checkFollow.php?email="+emailAutore+"&idDomanda="+ domandaItem.getId();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            seguito = jsonObject.getBoolean("result");
                            switchMaterial.setChecked(seguito);
                            check = true;
                        } catch (JSONException ex) {
                            //in caso di errore metto false
                            seguito=false;
                            switchMaterial.setChecked(seguito);
                            check = true;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){

        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }



    @Override
    public void onStart() {
        super.onStart();
        //registerNetworkCallback();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();
        connection.close();
    }

    @Override
    public void onItemClick(int position) {
        sharedPreferences = activity.getSharedPreferences(Utilities.KEY_RISPOSTA1SELEZIONATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
        Gson gson = new Gson();
        String json = gson.toJson(AnswerCardItemSINGLETON.getInstance().getCardRispostaItem(position));
        editor.putString(Utilities.KEY_RISPOSTA1SELEZIONATA,json);
        editor.apply(); //NECESSARIO

        HomeActivity activity = (HomeActivity)getActivity();
        if(activity != null)
            activity.addIdInBackList(activity.getLastItemInBackList());
        Utilities.insertFragment(activity, new AnswerTwoFragment(), "AnswerTwoFragment", R.id.fragment_container_home, true);
    }

    /**
     * metodo chiamato per aggiornare la lista
     */
    void updateList() {
        Log.d(LOG, "updateList()");
        adapter.updateList();
    }

}
