package com.example.edengardens.RecyclerView.Notification;

import java.util.ArrayList;
import java.util.List;

public class NotificationCardItemSINGLETON {
    private List<NotificationCardItem> notificationCardItemList =new ArrayList<>();
    private static final NotificationCardItemSINGLETON ourInstance = new NotificationCardItemSINGLETON();


    public static NotificationCardItemSINGLETON getInstance(){
        return ourInstance;
    }

    private NotificationCardItemSINGLETON(){
    }

    public void addCardItemNotifica(NotificationCardItem cardItemNotifica){
        this.notificationCardItemList.add(cardItemNotifica);
    }

    public List<NotificationCardItem> getNotificheList(){return this.notificationCardItemList;}

    public NotificationCardItem getCardNotificaItem(int position){
        return notificationCardItemList.get(position);
    }

    public void emptyList(){this.notificationCardItemList.clear();}

    public void deleteNotifica(int position){
        notificationCardItemList.remove(position);
    }
}
