package com.example.edengardens.RecyclerView.Category;


public class CategoryCardItem {
    private int id;
    private String nome;


    public CategoryCardItem(int id, String nome){
        this.id=id;
        this.nome=nome;
    }


    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
}
