package com.example.edengardens.HomeActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Question.QuestionCardItemSINGLETON;
import com.example.edengardens.Utilities;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private static final String HOME_FRAGMENT_TAG = "homeFragment";

    private long backPressedTime;

    private BottomNavigationView bottomNavigationView;
    private Toast backToast;
    private List<Integer> backList = new ArrayList<>();
    private boolean back = true;

    private static final String TAG = "HOME_REQUEST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //svuoto le liste
        QuestionCardItemSINGLETON.getInstance().emptyList();

        Utilities.insertFragment(this, new HomeFragment(), HOME_FRAGMENT_TAG, R.id.fragment_container_home, false);

        backList.add(R.id.icona_home);

        bottomNavigationView=findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(!back){
                    switch (item.getItemId()){
                        case R.id.icona_home:
                            Utilities.insertFragment(HomeActivity.this, new HomeFragment(), HOME_FRAGMENT_TAG, R.id.fragment_container_home, true);
                           break;
                        case R.id.icona_follow:
                            Utilities.insertFragment(HomeActivity.this, new FollowFragment(), "FollowFragment", R.id.fragment_container_home,true);
                            break;
                        case R.id.icona_analisi:
                            Utilities.insertFragment(HomeActivity.this, new ScanDiseaseFragment(), "AnalisiFragment", R.id.fragment_container_home,true);
                            break;
                        case R.id.icona_cerca:
                            Utilities.insertFragment(HomeActivity.this, new SearchCategoryFragment(), "SearchCategoryFragment", R.id.fragment_container_home,true);
                            break;
                        case R.id.icona_user:
                            Utilities.insertFragment(HomeActivity.this, new ProfileFragment(), "ProfileFragment", R.id.fragment_container_home,true);
                            break;
                    }

                    addIdInBackList(item.getItemId());
                }
                back = false;
                return true;
            }
        });
        bottomNavigationView.setSelectedItemId(R.id.icona_home);



        backToast = Toast.makeText(getBaseContext(), R.string.exitMessage, Toast.LENGTH_SHORT);
    }

    @Override
    public void onBackPressed() {
        FragmentManager mgr = getSupportFragmentManager();
        if (mgr.getBackStackEntryCount() == 0) {
            // non ci sono fragment nella coda

            if(backPressedTime + 2000 > System.currentTimeMillis()){
                backToast.cancel();
                super.onBackPressed();
                return;
            } else{
                backToast.show();
            }

            backPressedTime = System.currentTimeMillis();
        } else {
            mgr.popBackStack();
            if(backList.size() > 1){
                backList.remove(backList.size()-1);
                back = true;
                bottomNavigationView.setSelectedItemId(backList.get(backList.size()-1));
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult()");
        if (requestCode == Utilities.ACTIVITY_ADD_QUESTION && resultCode == RESULT_OK) {
            Log.d(TAG, "RESULT_OK");
            FragmentManager manager = getSupportFragmentManager();
            HomeFragment homeFragment = (HomeFragment) manager.findFragmentByTag(HOME_FRAGMENT_TAG);
            if (homeFragment != null) {
                Log.d(TAG, "updateList()");
                homeFragment.updateList();
            }
        }
    }

    public void addIdInBackList(int id){
        backList.add(id);
    }

    public int getLastItemInBackList(){
        return backList.get(backList.size()-1);
    }
}
