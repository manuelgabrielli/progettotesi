package com.example.edengardens.HomeActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class ScanDiseaseFragment extends PhotoFragment {

    private static final String SCAN_RESULT_TAG = "Scan_result";
    private AlertDialog errorAlert;
    private ImageView plantImageView;
    private Button cambiaFotoButton;
    private ImageButton takePictureImageButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.scan_disease_fragment, container, false);

        final FragmentActivity activity = getActivity();
        if (activity != null) {
            //cambio dimensione campo testuale nello spinner e inserisco voci
            ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(activity,
                    R.array.select_plant_scan, R.layout.text_spinner);
            typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            final Spinner spinner = view.findViewById(R.id.idScegliPiantaSpinner);
            spinner.setAdapter(typeAdapter);

            errorAlert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.problema)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            takePictureImageButton = view.findViewById(R.id.idScegliImmagineScanButton);
            plantImageView = view.findViewById(R.id.scanPiantaimageView);
            cambiaFotoButton = view.findViewById(R.id.idCambiaFotoScanButton);

            //storico malattie
            Button storicoMalattie = view.findViewById(R.id.idStoricoAnalisiPiante);

            storicoMalattie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HomeActivity activity = (HomeActivity) getActivity();
                    if (activity != null)
                        activity.addIdInBackList(activity.getLastItemInBackList());
                    Utilities.insertFragment(activity, new DiseaseHistoryFragment(), "DiseaseHistoryFragment", R.id.fragment_container_home, true);
                }
            });
            
            final AlertDialog choseImageDialog =  new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.inserisci_una_foto)
                    .setItems(R.array.change_image_question_array, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case 0: dispatchPictureIntent(activity, Utilities.REQUEST_IMAGE_CAPTURE);
                                    break;
                                case 1: dispatchPictureIntent(activity, Utilities.PICK_FROM_GALLERY);
                                    break;
                            }
                        }
                    }).create();

            cambiaFotoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choseImageDialog.show();
                }
            });

            takePictureImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choseImageDialog.show();
                }
            });

            final AlertDialog choosePhotoAlert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.inserire_immagine_scan)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            Button analizzaButton = view.findViewById(R.id.idAnalizzaScanButton);
            analizzaButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getImageName() != null){
                        HomeActivity homeActivity = (HomeActivity)activity;
                        homeActivity.addIdInBackList(homeActivity.getLastItemInBackList());

                        //salvo la foto nelle shared preferences
                        SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_SCAN_DISEASE, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
                        Gson gson = new Gson();
                        String json = gson.toJson(getImageBitmap());
                        editor.putString(Utilities.KEY_SCAN_DISEASE,json);
                        editor.putString(Utilities.SCAN_PLANT_DISEASE, spinner.getSelectedItem().toString());
                        editor.apply(); //NECESSARIO

                        Utilities.insertFragment(activity, new ScanDiseaseResultFragment(), SCAN_RESULT_TAG, R.id.fragment_container_home, true);
                    } else{
                        choosePhotoAlert.show();
                    }
                }
            });
        }
        return view;
    }


    /**
     * Funzione chiamata dopo che una fotografia viene fatta per salvarla nella galleria
     * e visualizzarla
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Uri photoUri = getPhotoUri();
            if(getPhotoUri() != null){
                setImageBitmap(Utilities.getImageBitmap(Objects.requireNonNull(getActivity()), photoUri));
                Bitmap bitmap = getImageBitmap();
                if (bitmap != null){
                    try {
                        saveImage(bitmap);

                        setPhoto();
                    } catch (IOException e) {
                        setImageBitmap(null);
                        errorAlert.show();
                        e.printStackTrace();
                    }

                }
            }
        }

        if (requestCode == Utilities.PICK_FROM_GALLERY && resultCode == RESULT_OK){
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = Objects.requireNonNull(getContext()).getContentResolver().openInputStream(imageUri);
                setImageBitmap(BitmapFactory.decodeStream(imageStream));

                //creo nome immagine
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                setImageName("JPEG_" + timeStamp + ".jpg");

                setPhoto();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                setImageName(null);
                errorAlert.show();
            }
        }
    }

    private void setPhoto(){
        plantImageView.setVisibility(View.VISIBLE);
        takePictureImageButton.setVisibility(View.GONE);
        cambiaFotoButton.setVisibility(View.VISIBLE);

        plantImageView.setImageBitmap(getImageBitmap());
    }
}
