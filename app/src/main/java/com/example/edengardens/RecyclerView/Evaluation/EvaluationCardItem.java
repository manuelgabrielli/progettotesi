package com.example.edengardens.RecyclerView.Evaluation;

import com.example.edengardens.Utilities;

import java.text.DecimalFormat;

public class EvaluationCardItem {
    private String malattia;
    private String immagine;
    private String utente;
    private boolean eliminata;
    private String pianta;
    private String percentuale;

    public EvaluationCardItem(String malattia, String immagine, String utente, String percentuale, boolean eliminata) {
        float p = Float.parseFloat(percentuale) * 100;
        DecimalFormat numberFormat = new DecimalFormat("#0.0#");
        this.percentuale = numberFormat.format(p) + "%";
        this.immagine = immagine;
        this.utente = utente;
        this.eliminata = eliminata;
        String[] parts = malattia.split(Utilities.LABELS_SPLITTER);
        this.malattia = parts[1];
        this.pianta = parts[0];
    }

    public String getMalattia() {
        return malattia;
    }

    public String getImmagine() {
        return immagine;
    }

    public String getUtente() {
        return utente;
    }

    public boolean isEliminata(){
        return eliminata;
    }

    public String getPianta() {
        return pianta;
    }

    public String getPercentuale() {
        return percentuale;
    }
}
