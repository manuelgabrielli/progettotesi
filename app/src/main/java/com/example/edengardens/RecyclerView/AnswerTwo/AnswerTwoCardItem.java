package com.example.edengardens.RecyclerView.AnswerTwo;

public class AnswerTwoCardItem {

    private String risposta;
    private String autore;
    private String data;


    public AnswerTwoCardItem(String risposta, String autore, String data){
        this.risposta = risposta;
        this.autore = autore;
        this.data = data;
    }

    public String getRisposta() {
        return risposta;
    }

    public String getAutore() {
        return autore;
    }

    public String getData() {
        return data;
    }
}
