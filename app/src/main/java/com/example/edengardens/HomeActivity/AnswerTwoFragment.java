package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Question.QuestionCardItem;
import com.example.edengardens.RecyclerView.AnswerTwo.AnswerTwoCardAdapter;
import com.example.edengardens.RecyclerView.AnswerTwo.AnswerTwoCardItem;
import com.example.edengardens.RecyclerView.AnswerTwo.AnswerTwoCardItemSINGLETON;
import com.example.edengardens.RecyclerView.Answer.AnswerCardItem;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class AnswerTwoFragment extends Fragment implements AnswerTwoCardAdapter.OnItemListener {
    private static final String TAG = "ANSWER2_REQUEST";

    private AnswerTwoCardAdapter adapter;

    private Connection connection;

    private ConstraintLayout progressBarLayout;

    private View view;
    private RatingBar ratingBar;

    private String username;

    private ConstraintLayout noRisposteLayout;

    private AnswerCardItem risposta;
    private AlertDialog problemAlert;

    private QuestionCardItem domanda;

    private FragmentActivity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.answer2_fragment, container, false);

        activity = getActivity();
        if(activity != null) {
            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, Activity.MODE_PRIVATE);


            username = sharedPreferences.getString(Utilities.KEY_USERNAME, "");

            sharedPreferences = activity.getSharedPreferences(Utilities.KEY_DOMANDASELEZIONATA, Activity.MODE_PRIVATE);
            Gson gson = new Gson();
            String json;
            String categoria = sharedPreferences.getString(Utilities.CATEGORIA_DOMANDA, "");
            //faccio lo switch per predere la domanda giusta in base da che fragment si arriva
            switch (categoria) {
                case Utilities.KEY_DOMANDASELEZIONATA_HOME:
                    json = sharedPreferences.getString(Utilities.KEY_DOMANDASELEZIONATA_HOME, "");
                    break;
                case Utilities.KEY_DOMANDASELEZIONATA_FOLLOW:
                    json = sharedPreferences.getString(Utilities.KEY_DOMANDASELEZIONATA_FOLLOW, "");
                    break;
                case Utilities.KEY_DOMANDASELEZIONATA_SEARCH:
                    json = sharedPreferences.getString(Utilities.KEY_DOMANDASELEZIONATA_SEARCH, "");
                    break;
                default:
                    json = "";
            }
            domanda = gson.fromJson(json, QuestionCardItem.class);

            sharedPreferences = activity.getSharedPreferences(Utilities.KEY_RISPOSTA1SELEZIONATA, Activity.MODE_PRIVATE);
            json = sharedPreferences.getString(Utilities.KEY_RISPOSTA1SELEZIONATA, "");
            risposta = gson.fromJson(json, AnswerCardItem.class);

            noRisposteLayout = view.findViewById(R.id.noRisposte2Layout);

            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);


            connection.setNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    connection.setNetworkConnected(true);
                    connection.dismissSnackbar();
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() { //questo metodo serve per far girare il codice sul thread principale, perchè solo li posso cambaire la view
                            createRequest();
                        }
                    });

                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    connection.setNetworkConnected(false);
                    connection.showSnackbar();
                }

            });

            //setto il text della domanda e risposta
            TextView domandaText = view.findViewById(R.id.idDomandaPrincipale);
            domandaText.setText(domanda.getDomanda());
            TextView autoreDomanda = view.findViewById(R.id.idAutoreDomandaPrincipale);
            autoreDomanda.setText(domanda.getAutore());
            TextView dataDomanda = view.findViewById(R.id.idDataDomandaPrincipale);
            dataDomanda.setText(Utilities.changeDateFormat(domanda.getDate()));

            TextView rispostaText = view.findViewById(R.id.idRispostaPrincipale);
            rispostaText.setText(risposta.getRisposta());
            TextView autoreRisposta = view.findViewById(R.id.idAutoreRispostaPrincipale);
            autoreRisposta.setText(risposta.getAutore());
            TextView dataRisposta = view.findViewById(R.id.idDataRisposta2Principale);
            dataRisposta.setText(Utilities.changeDateFormat(risposta.getData()));

            ratingBar = view.findViewById(R.id.ratingBarRisposta);

            setRatingRequest();

            final EditText editText = view.findViewById(R.id.rispondiEditText);

            view.findViewById(R.id.inviaRispostaButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (editText.getText().length() != 0) {
                        createWritingRequest(editText.getText().toString());
                        editText.getText().clear();
                    }
                }
            });

            progressBarLayout = view.findViewById(R.id.progressBarRisposta2Layout);

            problemAlert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.problema)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            connection.showSnackbar(); //faccio vidualizzare di default la snackbar che non c'è connessione

            //aggiungo click sui profili degli utenti
            if(!domanda.getAutore().equals(username)){
                autoreDomanda.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences sp =  activity.getSharedPreferences(Utilities.KEY_OTHER_USER_PROFILE, Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit(); //serve per aggiungere dati allo sP
                        editor.putString(Utilities.KEY_OTHER_USER_PROFILE, domanda.getAutore());
                        editor.apply(); //NECESSARIO
                        HomeActivity act = (HomeActivity)activity;
                        act.addIdInBackList(act.getLastItemInBackList());
                        Utilities.insertFragment(activity, new ProfileOtherUserFragment(), "ProfileOtherUserFragment", R.id.fragment_container_home, true);
                    }
                });
            }
            if(!risposta.getAutore().equals(username)){
                autoreRisposta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences sp =  activity.getSharedPreferences(Utilities.KEY_OTHER_USER_PROFILE, Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit(); //serve per aggiungere dati allo sP
                        editor.putString(Utilities.KEY_OTHER_USER_PROFILE, risposta.getAutore());
                        editor.apply(); //NECESSARIO
                        HomeActivity act = (HomeActivity)activity;
                        act.addIdInBackList(act.getLastItemInBackList());
                        Utilities.insertFragment(activity, new ProfileOtherUserFragment(), "ProfileOtherUserFragment", R.id.fragment_container_home, true);
                    }
                });
            }

        }
        return view;
    }

    private void setRatingRequest() {

        String url = "https://ftp.edengardens.altervista.org/rating.php?email="+ username +"&risposta="+ risposta.getId();

        //creiamo richiesta di tipo json con un array contenente almeno 1 elemento
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                try {

                    JSONObject jsonObject = response.getJSONObject(0);

                    //c'è il voto
                    ratingBar.setIsIndicator(true);
                    ratingBar.setRating((float)jsonObject.getDouble("voto"));

                } catch (JSONException e) {
                    e.printStackTrace();

                    //non c'è il voto
                    ratingBar.setIsIndicator(false);
                    ratingBar.setRating(0);
                    ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                        @Override
                        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                            createRatingRequest(rating);
                        }
                    });
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                Log.e(TAG, error.toString());
            }
        });

        //bisogna assegnarli un tag e inserirlo nella coda
        jsonArrayRequest.setTag(TAG);
        //requestQueue.add(jsonArrayRequest);
        connection.addRequest(jsonArrayRequest);
    }

    private void createRatingRequest(final float rating) {

        String url = "https://ftp.edengardens.altervista.org/rating.php?email="+ username +"&risposta="+ risposta.getId() +"&voto="+rating;

        //creiamo richiesta di tipo json con un array contenente almeno 1 elemento
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                try {

                    JSONObject jsonObject = response.getJSONObject(0);

                    if(!jsonObject.getBoolean("result")){
                        ratingBar.setRating(0);
                    } else{
                        //blocco la rating bar
                        ratingBar.setIsIndicator(true);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //c'è stato qualche problema
                    ratingBar.setRating(0);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                Log.e(TAG, error.toString());
            }
        });

        //bisogna assegnarli un tag e inserirlo nella coda
        jsonArrayRequest.setTag(TAG);
        //requestQueue.add(jsonArrayRequest);
        connection.addRequest(jsonArrayRequest);
    }

    private void createWritingRequest(final String text) {
        final String time = Utilities.getTime();

        String url = "https://ftp.edengardens.altervista.org/risposte2.php?risposta="+ text +"&data="+ time +"&email="+ username +"&idRisposta="+ risposta.getId()+"&idDomanda="+
                +domanda.getId()+"&emailUserRicevente="+risposta.getAutore();

        progressBarLayout.setVisibility(View.VISIBLE);

        //creiamo richiesta di tipo json con un array contenente almeno 1 elemento
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                try {

                    JSONObject jsonObject = response.getJSONObject(0);

                    //registrazione andata a buon fine
                    //se il risultato è false vuol dire che c'è già un utente con quella mail
                    if(!jsonObject.getBoolean("result")){
                        problemAlert.show();
                    } else {
                        AnswerTwoCardItem listItem =new AnswerTwoCardItem(text, username, Utilities.changeDateFormat(time));
                        AnswerTwoCardItemSINGLETON.getInstance().addListItem(listItem);
                        updateList();
                    }

                    progressBarLayout.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                    //c'è stato qualche problema
                    progressBarLayout.setVisibility(View.GONE);
                    problemAlert.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                Log.e(TAG, error.toString());
                problemAlert.show();
            }
        });

        //bisogna assegnarli un tag e inserirlo nella coda
        jsonArrayRequest.setTag(TAG);
        //requestQueue.add(jsonArrayRequest);
        connection.addRequest(jsonArrayRequest);
    }

    private void createRequest(){

        String url = "https://ftp.edengardens.altervista.org/risposte2.php?risposta="+risposta.getId();

        progressBarLayout.setVisibility(View.VISIBLE);

        AnswerTwoCardItemSINGLETON.getInstance().emptyList();

        //creiamo richiesta di tipo json con un array contenente almeno 1 elemento
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                try {
                    for(int i=0;i<response.length();i++){
                        String risposta = response.getJSONObject(i).getString("testoRisposta");
                        String autore = response.getJSONObject(i).getString("email");
                        String data = response.getJSONObject(i).getString("dataRisposta");
                        AnswerTwoCardItem listItem =new AnswerTwoCardItem(risposta, autore, Utilities.changeDateFormat(data));
                        AnswerTwoCardItemSINGLETON.getInstance().addListItem(listItem);
                    }

                    setRecyclerView(view);

                    progressBarLayout.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    //c'è stato qualche problema
                    progressBarLayout.setVisibility(View.GONE);
                    problemAlert.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                Log.e(TAG, error.toString());
            }
        });

        //bisogna assegnarli un tag e inserirlo nella coda
        jsonArrayRequest.setTag(TAG);
        //requestQueue.add(jsonArrayRequest);
        connection.addRequest(jsonArrayRequest);

    }

    private void setRecyclerView(View view){
        // Set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.idRisposta2RecyclerView);

        adapter = new AnswerTwoCardAdapter(AnswerTwoCardItemSINGLETON.getInstance().getList(), this);
        recyclerView.setAdapter(adapter);

        if(AnswerTwoCardItemSINGLETON.getInstance().getList().isEmpty()){
            //faccio vedere il layout della scritta
            noRisposteLayout.setVisibility(View.VISIBLE);
        } else {
            noRisposteLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo

        connection.close();
    }

    /**
     * metodo chiamato per aggiornare la lista
     */
    void updateList() {
        noRisposteLayout.setVisibility(View.GONE);
        adapter.updateList();
    }

    @Override
    public void onAuthorNameClick(int position) {
        String autore = AnswerTwoCardItemSINGLETON.getInstance().getListItem(position).getAutore();

        if (!username.equals(autore)) {
            SharedPreferences sp = activity.getSharedPreferences(Utilities.KEY_OTHER_USER_PROFILE, Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit(); //serve per aggiungere dati allo sP
            editor.putString(Utilities.KEY_OTHER_USER_PROFILE, autore);
            editor.apply(); //NECESSARIO
            HomeActivity act = (HomeActivity) activity;
            act.addIdInBackList(act.getLastItemInBackList());
            Utilities.insertFragment((FragmentActivity) activity, new ProfileOtherUserFragment(), "ProfileOtherUserFragment", R.id.fragment_container_home, true);
        }
    }
}
