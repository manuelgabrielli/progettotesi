package com.example.edengardens.RecyclerView.AnswerTwo;

import java.util.ArrayList;
import java.util.List;

public class AnswerTwoCardItemSINGLETON {
    private List<AnswerTwoCardItem> listItem =new ArrayList<>();
    private static final AnswerTwoCardItemSINGLETON ourInstance = new AnswerTwoCardItemSINGLETON();

    public static AnswerTwoCardItemSINGLETON getInstance(){
        return ourInstance;
    }

    private AnswerTwoCardItemSINGLETON(){ }

    public void addListItem(AnswerTwoCardItem listItem){
        this.listItem.add(listItem);
    }

    public List<AnswerTwoCardItem> getList(){
        return listItem;
    }

    public AnswerTwoCardItem getListItem(int position){
        return listItem.get(position);
    }

    public void emptyList(){
        listItem.clear();
    }
}
