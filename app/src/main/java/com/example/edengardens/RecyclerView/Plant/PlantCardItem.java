package com.example.edengardens.RecyclerView.Plant;


public class PlantCardItem {
    private int id;
    private String nome;
    private String immaginePath;
    private String descrizione;


    public PlantCardItem(int id, String nome, String descrizione, String immaginePath){
        this.id=id;
        this.nome=nome;
        this.descrizione = descrizione;
        this.immaginePath = immaginePath;
    }


    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getImmaginePath() {
        return immaginePath;
    }

    public String getDescrizione() {
        return descrizione;
    }
}
