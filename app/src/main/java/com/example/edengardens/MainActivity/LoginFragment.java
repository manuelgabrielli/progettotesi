package com.example.edengardens.MainActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.EvaluateActivity.EvaluateActivity;
import com.example.edengardens.HomeActivity.HomeActivity;
import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import se.simbio.encryption.Encryption;

public class LoginFragment extends Fragment {

    private static final String TAG = "LOGIN_REQUEST";

    private EditText email;
    private EditText password;

    private Connection connection;
    private Snackbar snackbarError;
    private Snackbar snackbar;
    private FragmentActivity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.login_fragment, container, false);

        activity = getActivity();
        if(activity != null) {
            View fragmentContainer = activity.findViewById(R.id.fragment_container);
            connection = new Connection(getContext(), fragmentContainer, activity, TAG);

            email = view.findViewById(R.id.emailEditText);
            password = view.findViewById(R.id.passwordEditText);
            snackbar = Snackbar.make(fragmentContainer, R.string.noUtenteOPassword, Snackbar.LENGTH_INDEFINITE).setAction("ok", (new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            }));

            snackbarError = Snackbar.make(fragmentContainer, R.string.utenteOPasswordErrati, Snackbar.LENGTH_INDEFINITE).setAction("ok", (new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbarError.dismiss();
                }
            }));

            TextView registration = view.findViewById(R.id.registratiButton);
            registration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utilities.insertFragment(activity, new RegistrationFragment(), "registrationFragment", R.id.fragment_container, true);
                }
            });

            Button login = view.findViewById(R.id.accediButton);
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (email.getText().length() == 0 || password.getText().length() == 0) {
                        snackbar.show();
                    } else {
                        if (connection.isConnected()) {
                            createRequest();
                        } else {
                            Log.e(TAG, "onClick: no connection");
                            connection.showSnackbar();
                        }

                    }


                }
            });

        }
        return view;
    }

    private void createRequest(){
        String url = "https://ftp.edengardens.altervista.org/login.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            boolean isAdmin = jsonObject.getInt("admin")==1;

                            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP

                            if(!isAdmin) {
                                //utente trovato
                                editor.putString(Utilities.KEY_USERNAME, email.getText().toString());
                                editor.putString(Utilities.KEY_CITTA, jsonObject.getString("citta"));
                                editor.putString(Utilities.KEY_NOME, jsonObject.getString("nome"));
                                editor.putString(Utilities.KEY_COGNOME, jsonObject.getString("cognome"));
                                editor.putString(Utilities.KEY_IMMAGINE_PROFILO, jsonObject.getString("immagine_profilo"));
                                editor.apply(); //NECESSARIO

                                Intent intent = new Intent(activity, HomeActivity.class);
                                startActivity(intent);
                            } else{
                                //admin trovato
                                editor.putString(Utilities.KEY_ADMIN, email.getText().toString());
                                editor.apply(); //NECESSARIO

                                Intent intent = new Intent(activity, EvaluateActivity.class);
                                startActivity(intent);
                            }
                            activity.finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            // non ha trovato l'utente
                            snackbarError.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("email",email.getText().toString());

                //cripto la password
                byte[] iv = new byte[16];
                Encryption encryption = Encryption.getDefault(Utilities.getKey(), Utilities.getSalt(), iv);
                String encrypted = encryption.encryptOrNull(password.getText().toString());
                if(encrypted.contains("\n")){
                    encrypted = encrypted.replace("\n", "");
                }
                params.put("password",encrypted);

                return params;
            }

        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }

    @Override
    public void onStart() {
        super.onStart();
        //registerNetworkCallback();
        connection.registerNetworkCallback();
    }

    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo

        connection.close();
    }
}
