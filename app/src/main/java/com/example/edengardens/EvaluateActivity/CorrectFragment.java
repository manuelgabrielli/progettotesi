package com.example.edengardens.EvaluateActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Evaluation.EvaluationCardItem;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.tensorflow.lite.support.common.FileUtil;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CorrectFragment extends Fragment {
    private static final String TAG = "VALIDATION_REQUEST";
    private boolean scelto = false;
    private boolean diseaseCorrect = false;
    private Connection connection;
    private ConstraintLayout progressBar;
    private AlertDialog problemAlert;
    private FragmentActivity activity;
    private Snackbar snackbar;
    private Spinner malattieSpinner;
    private TextInputLayout malattiaCorrettaLayout;
    private EditText malattiaCorrettaText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.correct_fragment, container, false);

        activity = getActivity();

        if(activity != null){
            connection = new Connection(getContext(), activity.findViewById(R.id.evaluateLayoutActivity), activity, TAG);
            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_VALUTAZIONESELEZIONATA, Activity.MODE_PRIVATE);
            Gson gson = new Gson();
            String json = sharedPreferences.getString(Utilities.KEY_VALUTAZIONESELEZIONATA, "");
            final EvaluationCardItem evaluationItem = gson.fromJson(json, EvaluationCardItem.class);

            ImageView immagine = view.findViewById(R.id.correzioneImageView);
            TextView malattia = view.findViewById(R.id.malattiaCorrezioneTextView);
            final Button correttoButton = view.findViewById(R.id.correttoButton);
            final Button sbagliatoButton = view.findViewById(R.id.sbagliatoButton);
            malattieSpinner = view.findViewById(R.id.idScegliMalattiaGiustaSpinner);
            Button confermaButton = view.findViewById(R.id.confermaButton);
            final View divider = view.findViewById(R.id.dividerCorrect);
            progressBar = view.findViewById(R.id.progressBarCorrezioneLayout);
            malattiaCorrettaLayout = view.findViewById(R.id.malattiaCorrettaInputLayout);
            malattiaCorrettaText = view.findViewById(R.id.malattiaCorrettaText);

            problemAlert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.problema)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            malattieSpinner.setEnabled(false);

            Picasso.get()
                    .load(Utilities.IMAGE_PREFIX + evaluationItem.getImmagine())
                    .resize(400, 400)
                    .onlyScaleDown()
                    .centerCrop()
                    .into(immagine);

            malattia.setText(evaluationItem.getMalattia());

            List<String> modelLabels;
            try {
                modelLabels = FileUtil.loadLabels(activity, "labelsModel.txt");
                modelLabels.add(0, Utilities.MALATTIA_NON_DISPONIBILE);

                askOtherLabels(modelLabels);

            } catch (IOException e) {
                problemAlert.show();
                confermaButton.setEnabled(false);
            }



            correttoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    diseaseCorrect = true;
                    correttoButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    sbagliatoButton.setBackgroundColor(getResources().getColor(R.color.gray));
                    malattieSpinner.setEnabled(false);
                    divider.setBackgroundColor(getResources().getColor(R.color.gray));
                    malattiaCorrettaText.setEnabled(false);
                    scelto = true;
                }
            });

            sbagliatoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    diseaseCorrect = false;
                    correttoButton.setBackgroundColor(getResources().getColor(R.color.gray));
                    sbagliatoButton.setBackgroundColor(getResources().getColor(R.color.wrong));
                    malattieSpinner.setEnabled(true);
                    malattiaCorrettaText.setEnabled(true);
                    divider.setBackgroundColor(getResources().getColor(R.color.wrong));
                    scelto = true;
                }
            });

            final AlertDialog problem = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.errore_malattia_inserita_errata)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();
            confermaButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(scelto) {
                        if (diseaseCorrect) {
                            sendCorrectionCorrect(evaluationItem);
                        } else {
                            //controllo se l'utente ha inserito una malattia nuova nel formato richiesto
                            if(malattieSpinner.getSelectedItem().toString().equals(getString(R.string.altro))){
                                String text = malattiaCorrettaText.getText().toString();
                                if(text.contains(Utilities.LABELS_SPLITTER)){
                                    //tutto ok
                                    sendCorrectionIncorrect(evaluationItem, true);
                                } else{
                                    problem.show();
                                }
                            } else{
                                sendCorrectionIncorrect(evaluationItem, false);
                            }
                        }
                    } else{
                        snackbar = Snackbar.make(activity.findViewById(R.id.evaluateLayoutActivity), R.string.seleziona_valutazione, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok, (new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        }));
                        snackbar.show();
                    }
                }
            });


        }

        return view;
    }

    private void setSpinnerAdapter(List<String> labels){
        labels.add(getString(R.string.altro));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), R.layout.list, labels);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        malattieSpinner.setAdapter(adapter);
        malattieSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //i è l'indice dell'elemento selezionato
                String s = malattieSpinner.getItemAtPosition(i).toString();
                if(s.equals(getString(R.string.altro))){
                    //faccio comparire la text box
                    malattiaCorrettaLayout.setVisibility(View.VISIBLE);
                } else{
                    //faccio scoparire la textbox
                    malattiaCorrettaLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void askOtherLabels(final List<String > labels) {
        String url = "https://ftp.edengardens.altervista.org/analisiPiante.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                labels.add(obj.getString("nome_malattia"));
                            }

                            //ordino la lista
                            Collections.sort(labels);
                            setSpinnerAdapter(labels);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //se c'è stato un errore carico solo le categorie del modello
                            setSpinnerAdapter(labels);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("admin", "true");
                params.put("labels", "true");
                return params;
            }
        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }

    private void sendCorrectionCorrect(final EvaluationCardItem evaluationItem) {
        String url = "https://ftp.edengardens.altervista.org/analisiPiante.php";

        progressBar.setVisibility(View.VISIBLE);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);


                            progressBar.setVisibility(View.GONE);

                            if(jsonObject.getBoolean("result")) {
                                Utilities.insertFragment(activity, new EvaluateFragment(), "evaluateFragment", R.id.evaluateLayoutActivity, false);
                            } else{
                                problemAlert.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                            problemAlert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("admin", "true");
                params.put("correct", "true");
                params.put("immagine", evaluationItem.getImmagine());
                params.put("utente", evaluationItem.getUtente());
                if(!evaluationItem.isEliminata()) {
                    params.put("notification", "true"); //invio questo parametro se devo inviare la notifica all'utente
                    params.put("orario", Utilities.getTime());
                }
                return params;
            }
        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }

    private void sendCorrectionIncorrect(final EvaluationCardItem evaluationItem, final boolean nuovaCategoria) {
        String url = "https://ftp.edengardens.altervista.org/analisiPiante.php";

        progressBar.setVisibility(View.VISIBLE);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);


                            progressBar.setVisibility(View.GONE);

                            if(jsonObject.getBoolean("result")) {
                                Utilities.insertFragment(activity, new EvaluateFragment(), "evaluateFragment", R.id.evaluateLayoutActivity, false);
                            } else{
                                problemAlert.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                            problemAlert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("admin", "true");
                params.put("incorrect", "true");
                params.put("immagine", evaluationItem.getImmagine());
                params.put("utente", evaluationItem.getUtente());
                if(nuovaCategoria){
                    params.put("nomeMalattia", malattiaCorrettaText.getText().toString());
                } else{
                    params.put("nomeMalattia", malattieSpinner.getSelectedItem().toString());
                }
                params.put("nuovaCategoria", String.valueOf(nuovaCategoria));
                if(!evaluationItem.isEliminata()) {
                    params.put("notification", "true"); //invio questo parametro se devo inviare la notifica all'utente
                    params.put("orario", Utilities.getTime());
                }

                return params;
            }
        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }

    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }

    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo
        connection.close();
    }
}
