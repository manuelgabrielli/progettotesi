package com.example.edengardens.RecyclerView.Evaluation;

import com.example.edengardens.RecyclerView.Plant.PlantCardItem;

import java.util.ArrayList;
import java.util.List;

public class EvaluationCardItemSINGLETON {
    private List<EvaluationCardItem> cardItemEvaluationList = new ArrayList<>();
    private static final EvaluationCardItemSINGLETON ourInstance = new EvaluationCardItemSINGLETON();

    public static EvaluationCardItemSINGLETON getInstance(){
        return ourInstance;
    }

    private EvaluationCardItemSINGLETON(){
    }

    public void addCardItemValutazione(EvaluationCardItem evaluationCardItem){
        this.cardItemEvaluationList.add(evaluationCardItem);
    }

    public void setList(List<EvaluationCardItem> list){
        this.cardItemEvaluationList = new ArrayList<>(list);
    }

    public List<EvaluationCardItem> getValutazioneList(){
        return this.cardItemEvaluationList;
    }

    public EvaluationCardItem getCardValutazioneItem(int position){
        return cardItemEvaluationList.get(position);
    }

    public void EmptyList(){
        this.cardItemEvaluationList.clear();
    }

    public void deleteValutazione(int position){
        this.cardItemEvaluationList.remove(position);
    }
}
