package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ProfileOtherUserFragment extends ProfileFragment {
    private static final String TAG = "OTHER_USER-PROFILE";
    private String emailUser;
    private TextView nome;
    private TextView cognome;
    private TextView citta;
    private TextView emailProfilo;
    private TextView viveProfilo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        Activity activity = getActivity();
        if(view != null && activity != null) {
            nome = view.findViewById((R.id.idNomeProfilo));
            cognome = view.findViewById(R.id.idCognomeProfilo);
            TextView email = view.findViewById(R.id.idEmailProfilo);
            citta = view.findViewById(R.id.idCittaProfilo);

            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_OTHER_USER_PROFILE, Context.MODE_PRIVATE);
            emailUser = sharedPreferences.getString(Utilities.KEY_OTHER_USER_PROFILE, "");

            email.setText(emailUser);

            Button logout = view.findViewById(R.id.idLogoutButton);
            logout.setVisibility(View.GONE);

            Button changeImage = view.findViewById(R.id.idCambiaFotoButton);
            changeImage.setVisibility(View.GONE);

            emailProfilo = view.findViewById(R.id.emailProfilotextView);
            viveProfilo = view.findViewById(R.id.viveAProfilotextView);

            //setto vuoti i campi
            Picasso.get().load(R.drawable.user_black).into(profiloImageView);
            nome.setText("");
            cognome.setText("");
            citta.setText("");

            getUserInformation();

            TextView titoloProfilo = view.findViewById(R.id.profiloUsertextView);
            titoloProfilo.setVisibility(View.GONE);
            ImageView titoloOtherProfilo = view.findViewById(R.id.titleProfiloOtherUserImageView);
            titoloOtherProfilo.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private void getUserInformation() {
        String url = "https://ftp.edengardens.altervista.org/profilo.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            nome.setText(jsonObject.getString("nome"));
                            cognome.setText(jsonObject.getString("cognome"));
                            citta.setText(jsonObject.getString("citta"));
                            String immagine = jsonObject.getString("immagine_profilo");
                            if (!immagine.equals("null")) {
                                Picasso.get()
                                        .load(Utilities.IMAGE_PREFIX + jsonObject.getString("immagine_profilo"))
                                        .fit()
                                        .centerCrop()
                                        .into(profiloImageView);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                            emailProfilo.setVisibility(View.GONE);
                            viveProfilo.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("otherUser", emailUser);

                return params;
            }
        };
        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }
}
