package com.example.edengardens.RecyclerView.Notification;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class NotificationCardAdapter extends RecyclerView.Adapter<NotificationCardAdapter.CardViewHolder>{
    private List<NotificationCardItem> cardItemList;

    private OnItemListener listener;

    public NotificationCardAdapter(List<NotificationCardItem> cardItemList, OnItemListener listener) {
        this.cardItemList = new ArrayList<>(cardItemList);
        this.listener = listener;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_layout, parent, false);
        return new CardViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        NotificationCardItem currentCard = cardItemList.get(position);
        holder.testo.setText(currentCard.getTesto());
        holder.date.setText(Utilities.changeDateFormat(currentCard.getData()));
        if(currentCard.isImagePresent()){
            Picasso.get()
                    .load(Utilities.IMAGE_PREFIX + currentCard.getImmagine())
                    .resize(70, 70)
                    .centerCrop()
                    .into(holder.immagine);
        } else{
            if(currentCard.isAnalisiPiantaPresent()){
                Picasso.get()
                        .load(Utilities.IMAGE_PREFIX + currentCard.getIdAnalisiPianta())
                        .resize(70, 70)
                        .centerCrop()
                        .into(holder.immagine);
            } else{
                holder.immagine.setImageResource(R.drawable.user_black);
            }
        }

    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }

    public void updateList() {
        cardItemList.clear();
        cardItemList.addAll(NotificationCardItemSINGLETON.getInstance().getNotificheList());
        notifyDataSetChanged();
    }

    static class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView testo;
        TextView date;
        ImageView immagine;

        OnItemListener itemListener;

        public CardViewHolder(@NonNull View itemView, OnItemListener lister) {
            super(itemView);
            testo = itemView.findViewById(R.id.idTestoNotifica);
            date = itemView.findViewById(R.id.idDataNotifica);
            immagine = itemView.findViewById(R.id.profiloNotificaImage);
            itemListener = lister;

            ImageButton button = itemView.findViewById(R.id.cancelNotificationImageButton);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //handler pulsante per cancellare la notifica
                    itemListener.onButtonDeleteNotificationClick(getAdapterPosition(), v);
                }
            });

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemListener{
        void onItemClick(int position);
        void onButtonDeleteNotificationClick(int position, View v);
    }
}
