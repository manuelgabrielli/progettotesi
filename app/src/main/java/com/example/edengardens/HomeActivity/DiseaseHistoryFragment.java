package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.DiseaseHistory.HistoryCardAdapter;
import com.example.edengardens.RecyclerView.DiseaseHistory.HistoryCardItem;
import com.example.edengardens.RecyclerView.DiseaseHistory.HistoryCardItemSINGLETON;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.tensorflow.lite.support.common.FileUtil;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class DiseaseHistoryFragment extends Fragment implements HistoryCardAdapter.OnItemListener, PopupMenu.OnMenuItemClickListener{

    private static final String TAG = "DisHistoryFragmentTAG";
    private static final int INDICE_NO_MALATTIA = 0;
    private Connection connection;
    private ConstraintLayout noMalattieInArchivioLayout;
    private ConstraintLayout progressBarLayout;
    private String email;
    private AlertDialog alertDialog;
    private int positionDelete;
    private HistoryCardAdapter adapter;
    private String piantaSanaNome;
    private List<String> labels;
    private List<String> modelLabels;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view=inflater.inflate(R.layout.diseases_history_fragment,container,false);

        activity = getActivity();
        if(activity != null) {
            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);

            noMalattieInArchivioLayout = view.findViewById(R.id.noMalattieInArchivioLayout);
            progressBarLayout = view.findViewById(R.id.progressBarArchivioLayout);

            //svuoto la lista dell'archivio
            HistoryCardItemSINGLETON.getInstance().emptyList();

            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, MODE_PRIVATE);
            email = sharedPreferences.getString(Utilities.KEY_USERNAME,"");

            alertDialog = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.problema)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            //leggo le labels per la traduzione dei nomi delle malattie
            String language = Locale.getDefault().getLanguage();
            try {
                //da togliere commento quando verrà inserita la lingua inglese
                /*switch (language){
                    case "it": labels = FileUtil.loadLabels(activity, "labelsIta.txt");
                        labels.add(0, getResources().getString(R.string.malattia_non_rilevabile));
                        piantaSanaNome = getResources().getString(R.string.pianta_sana);
                        break;
                    default: labels = FileUtil.loadLabels(activity, "labelsEng.txt"); //lingua inglese
                        labels.add(0, getResources().getString(R.string.malattia_non_rilevabile));
                        piantaSanaNome = getResources().getString(R.string.pianta_sana);
                }*/
                //solo con lingua italiana
                labels = FileUtil.loadLabels(activity, "labelsIta.txt");
                labels.add(INDICE_NO_MALATTIA, getResources().getString(R.string.malattia_non_rilevabile));
                piantaSanaNome = getResources().getString(R.string.pianta_sana);

                /* --- */
                modelLabels = FileUtil.loadLabels(activity, "labelsModel.txt");
                modelLabels.add(INDICE_NO_MALATTIA, Utilities.MALATTIA_NON_DISPONIBILE);
            }  catch (IOException e) {
                e.printStackTrace();
            }
            connection.setNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    connection.setNetworkConnected(true);
                    connection.dismissSnackbar();
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() { //questo metodo serve per far girare il codice sul thread principale, perchè solo li posso cambaire la view
                            askHistory(view);
                        }
                    });

                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    connection.setNetworkConnected(false);
                    connection.showSnackbar();
                }

            });

            connection.showSnackbar();
        }
        return view;
    }

    private void askHistory(final View view){
        String url = "https://ftp.edengardens.altervista.org/analisiPiante.php?email="+email;

        HistoryCardItemSINGLETON.getInstance().emptyList();

        progressBarLayout.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String malattiaArchivio = obj.getString("nome_malattia");
                                boolean validata = obj.getInt("validata") != 0;
                                boolean corretta = obj.getInt("corretta") != 0;
                                String immagine = obj.getString("immagine_pianta");
                                boolean nuovaCategoria = obj.getInt("nuova_categoria") != 0;

                                String m;
                                if(nuovaCategoria){
                                    m= labels.get(INDICE_NO_MALATTIA);
                                } else{
                                    m = labels.get(modelLabels.indexOf(malattiaArchivio)); //prendo il nome della malattia tradotto

                                }

                                try {
                                    String[] parts = m.split(Utilities.LABELS_SPLITTER);
                                    String pianta = parts[0];
                                    String disease = parts[1];

                                    if(malattiaArchivio.contains(Utilities.MODEL_LABEL_HEALTHY)){
                                        disease = piantaSanaNome;
                                    }
                                    HistoryCardItem historyCardItem = new HistoryCardItem(malattiaArchivio, immagine, corretta, validata, pianta, disease);

                                    HistoryCardItemSINGLETON.getInstance().addCardItemMalattia(historyCardItem);
                                } catch (Exception e){
                                    //malattia non rilevabile
                                    HistoryCardItem historyCardItem = new HistoryCardItem(malattiaArchivio, immagine, corretta, validata, getString(R.string.nessuna), m);

                                    HistoryCardItemSINGLETON.getInstance().addCardItemMalattia(historyCardItem);
                                }


                            }

                            progressBarLayout.setVisibility(View.GONE);

                            setRecyclerView(view);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBarLayout.setVisibility(View.GONE);
                            alertDialog.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        alertDialog.show();
                    }
                }){
        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }

    private void setRecyclerView(View view){
        RecyclerView recyclerView = view.findViewById(R.id.idStoricoMalattieRecyclerView);

        adapter = new HistoryCardAdapter(HistoryCardItemSINGLETON.getInstance().getMalattieList(),this);
        recyclerView.setAdapter(adapter);

        if(HistoryCardItemSINGLETON.getInstance().getMalattieList().isEmpty()){
            noMalattieInArchivioLayout.setVisibility(View.VISIBLE);
        } else{
            noMalattieInArchivioLayout.setVisibility(View.GONE);
        }
    }



    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo
        connection.close();
    }

    @Override
    public void onItemClick(int position) {
        //apro la versione estesa del risultato
        HistoryCardItem historyCardItem = HistoryCardItemSINGLETON.getInstance().getCardMalattieItem(position);

        SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_HISTORY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
        Gson gson = new Gson();
        String json = gson.toJson(historyCardItem);
        editor.putString(Utilities.KEY_HISTORY_ELEMENT,json);
        editor.apply(); //NECESSARIO

        HomeActivity activity = (HomeActivity)getActivity();
        if(activity != null)
            activity.addIdInBackList(activity.getLastItemInBackList());

        Utilities.insertFragment(activity, new DiseaseHistoryElementFragment(), "DiseaseHistoryElementFragment", R.id.fragment_container_home, true);

    }

    @Override
    public void onButtonDeleteDiseaseClick(int position, View v) {
        positionDelete = position;

        PopupMenu popupMenu = new PopupMenu(Objects.requireNonNull(getContext()), v);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.inflate(R.menu.disease_menu);
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.itemDeleteHistory) {
            deleteDisease();
            return true;
        }
        return false;
    }

    private void deleteDisease() {
        HistoryCardItem item = HistoryCardItemSINGLETON.getInstance().getCardMalattieItem(positionDelete);

        String url = "https://ftp.edengardens.altervista.org/analisiPiante.php?email="+email+"&voce_archivio="+item.getImmaginePianta();


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            JSONObject obj = jsonArray.getJSONObject(0);

                            if(obj.getBoolean("result")) {
                                HistoryCardItemSINGLETON.getInstance().deleteMalattia(positionDelete);
                                adapter.updateList();
                                if(HistoryCardItemSINGLETON.getInstance().getMalattieList().isEmpty()){
                                    noMalattieInArchivioLayout.setVisibility(View.VISIBLE);
                                }
                            } else{
                                alertDialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            alertDialog.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        alertDialog.show();
                    }
                }){
        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }
}
