package com.example.edengardens.RecyclerView.Question;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edengardens.HomeActivity.FollowFragment;
import com.example.edengardens.HomeActivity.HomeFragment;
import com.example.edengardens.HomeActivity.PlantQuestionsFragment;
import com.example.edengardens.R;
import com.example.edengardens.Utilities;


public class QuestionCardAdapter extends RecyclerView.Adapter<QuestionCardAdapter.CardViewHolder> implements Filterable {

    private static final String LOG = "CardAdapter-LAB";
    //list that can be filtered
    private List <QuestionCardItem> cardItemList;
    //list that contains ALL the element added by the user
    private List <QuestionCardItem> cardItemListFull;

    //listener attached to the onclick event for the item in yhe RecyclerView
    private OnItemListener listener;

    private Fragment fragment;

    public QuestionCardAdapter(List<QuestionCardItem> cardItemList, OnItemListener listener, Fragment fragment) {
        this.cardItemList = new ArrayList<>(cardItemList);
        this.cardItemListFull = new ArrayList<>(cardItemList);
        this.listener = listener;
        this.fragment = fragment;
    }


    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.preview_question_layout, parent, false);
        return new CardViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        QuestionCardItem currentCardItem = cardItemList.get(position);
        holder.titolo.setText(currentCardItem.getTitolo());
        holder.autore.setText(currentCardItem.getAutore());
        holder.fiore.setText(currentCardItem.getFiore());
        holder.date.setText(Utilities.changeDateFormat(currentCardItem.getDate()));
    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }


    @Override
    public Filter getFilter() {
        return cardFilter;
    }

    public void updateList() {
        Log.d(LOG, "updateList()");
        cardItemList.clear();
        cardItemList.addAll(QuestionCardItemSINGLETON.getInstance().getListDomanda());
        notifyDataSetChanged();
    }


    static class CardViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        TextView titolo;
        TextView date;
        TextView fiore;
        TextView autore;


        OnItemListener itemListener;

        CardViewHolder(@NonNull View itemView, OnItemListener lister) {
            super(itemView);
            titolo=itemView.findViewById(R.id.idTitoloDomandaPreview);
            fiore=itemView.findViewById(R.id.idCategoriaDomandaPreview);
            autore=itemView.findViewById(R.id.idAutoreDomandaPreview);
            date=itemView.findViewById(R.id.idDataDomanda);

            itemListener = lister;


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.onItemClick(getAdapterPosition());
        }
    }

    private Filter cardFilter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<QuestionCardItem> filteredList = new ArrayList<>();

            //if you have no constraint --> return the full list
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(cardItemListFull);
            } else {
                //else apply the filter and return a filtered list
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (QuestionCardItem item : cardItemListFull) {
                    if (item.getTitolo().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            cardItemList.clear();

            List<?> result = (List<?>) results.values;
            for (Object object : result) {
                if (object instanceof QuestionCardItem) {
                    cardItemList.add((QuestionCardItem) object);
                }
            }

            if (fragment instanceof HomeFragment) {
                QuestionCardItemSINGLETON.getInstance().setFilteredQuestionCardItemList(cardItemList);
            } else if(fragment instanceof PlantQuestionsFragment){
                QuestionCardItemSINGLETON.getInstance().setPlantQuestionsCardItemList(cardItemList);
            }else if(fragment instanceof FollowFragment){
                QuestionCardItemSINGLETON.getInstance().setFollowCardItemList(cardItemList);
            }
                //warn the adapter that the dare are changed after the filtering
            notifyDataSetChanged();
        }
    };


    public interface OnItemListener{
        void onItemClick(int position);
    }

}
