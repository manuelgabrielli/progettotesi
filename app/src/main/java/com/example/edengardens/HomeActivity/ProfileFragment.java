package com.example.edengardens.HomeActivity;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.MainActivity.MainActivity;
import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends PhotoFragment {

    private static final String TAG = "PROFILE_REQUEST";
    private SharedPreferences sharedPreferences;
    protected ImageView profiloImageView;
    protected Connection connection;
    private String emailUser;
    private AlertDialog errorAlert;
    private String immagine;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view=inflater.inflate(R.layout.profile_fragment,container,false);

        final Activity activity = getActivity();
        if(activity != null){
            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);

            TextView nome = view.findViewById((R.id.idNomeProfilo));
            TextView cognome = view.findViewById(R.id.idCognomeProfilo);
            TextView email = view.findViewById(R.id.idEmailProfilo);
            TextView citta = view.findViewById(R.id.idCittaProfilo);
            profiloImageView = view.findViewById(R.id.profile_image);


            sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, Context.MODE_PRIVATE);

            Button logout = view.findViewById(R.id.idLogoutButton);

            //logout
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.apply();

                    Intent intent = new Intent(activity, MainActivity.class);
                    startActivity(intent);
                    activity.finish();
                }
            });

            errorAlert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.problema)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            final AlertDialog choseImageDialog =  new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.cambia_foto_profilo)
                    .setItems(R.array.change_image_profile_array, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item
                            switch (which){
                                case 0: dispatchPictureIntent(activity, Utilities.REQUEST_IMAGE_CAPTURE);
                                    break;
                                case 1: dispatchPictureIntent(activity, Utilities.PICK_FROM_GALLERY);
                                    break;
                                case 2: deleteImage(activity);
                                    break;
                            }
                        }
                    }).create();

            Button changeImage = view.findViewById(R.id.idCambiaFotoButton);
            changeImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choseImageDialog.show();
                }
            });

            nome.setText(sharedPreferences.getString(Utilities.KEY_NOME, ""));
            cognome.setText(sharedPreferences.getString(Utilities.KEY_COGNOME, ""));
            citta.setText(sharedPreferences.getString(Utilities.KEY_CITTA, ""));
            emailUser = sharedPreferences.getString(Utilities.KEY_USERNAME, "");
            email.setText(emailUser);
            immagine = sharedPreferences.getString(Utilities.KEY_IMMAGINE_PROFILO, "");

            if(!immagine.equals("null")){
                Picasso.get()
                        .load(Utilities.IMAGE_PREFIX + immagine)
                        .fit()
                        .centerCrop()
                        .into(profiloImageView);
            }
        }
        return view;
    }

    private void uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        Bitmap bitmap = getImageBitmap();
        bitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY, byteArrayOutputStream);

        //rappresentazione in stringa dell'immagine
        final String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

        String url = "https://ftp.edengardens.altervista.org/savePicture.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            //qualcosa è andato storto
                            if(!jsonObject.getBoolean("result")) {
                                errorAlert.show();
                            } else{
                                changeUserImage();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            errorAlert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("nome",getImageName());
                params.put("immagine", encodedImage);

                return params;
            }
        };
        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }


    private void changeUserImage() {
        String url = "https://ftp.edengardens.altervista.org/profilo.php";
        final String imageName = getImageName();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            //qualcosa è andato storto
                            if(!jsonObject.getBoolean("result")) {
                                errorAlert.show();
                            } else{
                                //visualizzo l'immagine
                                profiloImageView.setImageBitmap(getImageBitmap());

                                //salvo in shared preferences
                                SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(Utilities.KEY_LOGIN, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Utilities.KEY_IMMAGINE_PROFILO, jsonObject.getString("immagine"));
                                editor.apply(); //NECESSARIO*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("immagine",imageName);
                params.put("email",emailUser);

                return params;
            }
        };
        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }

    /**
     * Funzione chiamata dopo che una fotografia viene fatta per salvarla nella galleria
     * e visualizzarla
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Uri photoUri = getPhotoUri();
            if(photoUri != null){
                setImageBitmap(Utilities.getImageBitmap(Objects.requireNonNull(getActivity()), photoUri));
                if (getImageBitmap() != null){
                    try {
                        saveImage(getImageBitmap());

                        //salvo immagine sul server
                        uploadImage();
                    } catch (IOException e) {
                        errorAlert.show();
                        e.printStackTrace();
                    }
                }
            }
        }

        if (requestCode == Utilities.PICK_FROM_GALLERY && resultCode == RESULT_OK){
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = Objects.requireNonNull(getContext()).getContentResolver().openInputStream(imageUri);
                setImageBitmap(BitmapFactory.decodeStream(imageStream));

                //creo nome immagine
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                setImageName("JPEG_" + timeStamp + ".jpg");

                //salvo immagine sul server
                uploadImage();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                errorAlert.show();
            }
        }
    }


    private void deleteImage(final Activity activity) {
        if(!immagine.equals("null")){
            if(connection.isConnected()){
                String url = "https://ftp.edengardens.altervista.org/profilo.php?email="+emailUser+"&cancellaFoto=true";

                //creo richiesta al server
                final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                        try {
                            JSONObject jsonObject = response.getJSONObject(0);
                            if(jsonObject.getBoolean("result")){
                                SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Utilities.KEY_IMMAGINE_PROFILO, "null");
                                editor.apply(); //NECESSARIO

                                profiloImageView.setImageResource(R.drawable.user_black);
                            }else{
                                errorAlert.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            errorAlert.show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                        Log.e(TAG, error.toString());
                        errorAlert.show();
                    }
                });


                //bisogna assegnarli un tag e inserirlo nella coda
                jsonArrayRequest.setTag(TAG);

                connection.addRequest(jsonArrayRequest);
            } else {
                Log.e(TAG, "onClick: no connection");
                connection.showSnackbar();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        connection.registerNetworkCallback();
    }

    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo
        connection.close();
    }
}
