package com.example.edengardens.HomeActivity;

import com.example.edengardens.Utilities;

//classe per salvare il risultato della predizione della malattia di una pianta
public class ScanResult {

    //nome malattia del modello
    private final String modelDisease;

    //nome della malattia
    private final String disease;

    private final String plant;

    //indica la percentuale di sicurezza della predizione
    private final Float confidence;

    public ScanResult(final String disease, final Float confidence, final String modelDisease) {
        this.confidence = confidence;
        String[] parts = disease.split(Utilities.LABELS_SPLITTER);
        this.plant = parts[0];
        this.disease = parts[1];
        this.modelDisease = modelDisease;
    }

    public String getDisease() {
        return disease;
    }

    public String getModelDisease() {
        return modelDisease;
    }

    public Float getConfidence() {
        return confidence;
    }


    @Override
    public String toString() {
        String resultString = "";

        if (plant != null) {
            resultString += plant + " ";
        }

        if (disease != null) {
            resultString += disease + " ";
        }

        if (confidence != null) {
            resultString += String.format("(%.1f%%) ", confidence * 100.0f);
        }

        return resultString.trim();
    }
}
