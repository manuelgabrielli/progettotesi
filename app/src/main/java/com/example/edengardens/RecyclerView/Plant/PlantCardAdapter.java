package com.example.edengardens.RecyclerView.Plant;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class PlantCardAdapter extends RecyclerView.Adapter<PlantCardAdapter.CardViewHolderPianta> implements Filterable {

    private static final String LOG = "CardAdapter-LAB";
    //list that can be filtered
    private List <PlantCardItem> cardItemList;
    //list that contains ALL the element added by the user
    private List <PlantCardItem> cardItemListFull;

    //listener attached to the onclick event for the item in yhe RecyclerView
    private OnItemListener listener;

    public PlantCardAdapter(List<PlantCardItem> cardItemList, OnItemListener listener) {
        this.cardItemList = new ArrayList<>(cardItemList);
        this.cardItemListFull = new ArrayList<>(cardItemList);
        this.listener = listener;
    }


    @NonNull
    @Override
    public CardViewHolderPianta onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.preview_plant_layout, parent, false);
        return new CardViewHolderPianta(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolderPianta holder, int position) {
        PlantCardItem currentCardItem = cardItemList.get(position);
        holder.nome.setText(currentCardItem.getNome());

        //mette l'immagine con la libreria picasso
        Picasso.get()
                .load(Utilities.IMAGE_PREFIX + currentCardItem.getImmaginePath())
                .resize(200, 200)
                .centerCrop()
                .into(holder.immagine);
    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }


    @Override
    public Filter getFilter() {
        return cardFilter;
    }


    static class CardViewHolderPianta extends RecyclerView.ViewHolder  implements View.OnClickListener {

        TextView nome;
        ImageView immagine;

        OnItemListener itemListener;

        CardViewHolderPianta(@NonNull View itemView, OnItemListener lister) {
            super(itemView);
            nome=itemView.findViewById(R.id.idTitoloPiantaPreview);
            immagine = itemView.findViewById(R.id.idPiantaImage);
            itemListener = lister;


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.onItemClick(getAdapterPosition());
        }
    }

    private Filter cardFilter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PlantCardItem> filteredList = new ArrayList<>();

            //if you have no constraint --> return the full list
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(cardItemListFull);
            } else {
                //else apply the filter and return a filtered list
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (PlantCardItem item : cardItemListFull) {
                    if (item.getNome().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            cardItemList.clear();

            List<?> result = (List<?>) results.values;
            for (Object object : result) {
                if (object instanceof PlantCardItem) {
                    cardItemList.add((PlantCardItem) object);
                }
            }

            PlantCardItemSINGLETON.getInstance().setList(cardItemList);
            //warn the adapter that the dare are changed after the filtering
            notifyDataSetChanged();
        }
    };


    public interface OnItemListener{
        void onItemClick(int position);
    }

}
