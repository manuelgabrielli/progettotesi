package com.example.edengardens.RecyclerView.Answer;

public class AnswerCardItem {
    private int id;
    private String autore;
    private String risposta;
    private String data;
    private float raking;

    public AnswerCardItem(int id, String autore, String risposta, String data, float raking){
        this.id=id;
        this.autore=autore;
        this.risposta=risposta;
        this.data=data;
        this.raking=raking;

    }

    public int getId(){return id;}
    public String getAutore(){return autore;}
    public  String getRisposta(){return risposta;}
    public  String getData(){return data;}
    public float getRaking(){return raking;}
}
