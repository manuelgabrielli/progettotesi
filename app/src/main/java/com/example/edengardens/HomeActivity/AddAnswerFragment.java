package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Question.QuestionCardItem;

import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;


public class AddAnswerFragment extends Fragment {
    private EditText risposta;
    private QuestionCardItem domandaItem;

    private Connection connection;
    private static final String TAG = "ADDRIPOSTA1_REQUEST";
    private AlertDialog connectionErrorAlert;
    private Activity activity;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        final View view = inflater.inflate(R.layout.add_answer1_layout, container, false);
        Button button = view.findViewById(R.id.idButtonRisposta1);
        TextView domanda = view.findViewById(R.id.idTestoDomanda1);
        risposta=view.findViewById(R.id.idTestoRisposta1);

        activity = getActivity();
        if(activity != null) {
            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);
            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_DOMANDASELEZIONATA, Activity.MODE_PRIVATE);
            Gson gson = new Gson();
            String json;
            String categoria = sharedPreferences.getString(Utilities.CATEGORIA_DOMANDA, "");
            //faccio lo switch per predere la domanda giusta in base da che fragment si arriva
            switch (categoria) {
                case Utilities.KEY_DOMANDASELEZIONATA_HOME:
                    json = sharedPreferences.getString(Utilities.KEY_DOMANDASELEZIONATA_HOME, "");
                    break;
                case Utilities.KEY_DOMANDASELEZIONATA_FOLLOW:
                    json = sharedPreferences.getString(Utilities.KEY_DOMANDASELEZIONATA_FOLLOW, "");
                    break;
                case Utilities.KEY_DOMANDASELEZIONATA_SEARCH:
                    json = sharedPreferences.getString(Utilities.KEY_DOMANDASELEZIONATA_SEARCH, "");
                    break;
                default:
                    json = "";
            }
            domandaItem = gson.fromJson(json, QuestionCardItem.class);
            
            String testoDomanda = domandaItem.getDomanda();

            domanda.setText(testoDomanda);

            //creo il messaggio di errore da visualizzare nel caso qualcosa andasse storto
            connectionErrorAlert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.errore_connessione)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            HomeActivity activity = (HomeActivity) getActivity();
                            if (activity != null)
                                activity.addIdInBackList(activity.getLastItemInBackList());
                            Utilities.insertFragment(activity, new ExpandedQuestionFragment(), "HomeFragment", R.id.fragment_container_home, true);

                        }
                    }).create();

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!risposta.getText().toString().equals("")) {
                        if (connection.isConnected()) {
                            createRequest(view);
                        } else{
                            Log.e(TAG, "onClick: no connection");
                            connection.showSnackbar();
                        }
                    }

                }
            });

        }

        return view;
    }



    private void createRequest(final View view){

        SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, MODE_PRIVATE);
        final String email=sharedPreferences.getString(Utilities.KEY_USERNAME,"");

        final int idDomanda = domandaItem.getId();

        final String data = Utilities.getTime();
        final int punteggioTotale=0,numeroValutazioni=0;

        final String testo=risposta.getText().toString();

        String url = "https://ftp.edengardens.altervista.org/addRisposta1.php";


        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            if(jsonObject.getBoolean("result")) {
                                HomeActivity activity = (HomeActivity) getActivity();
                                if (activity != null)
                                    activity.addIdInBackList(activity.getLastItemInBackList());
                                Utilities.insertFragment(activity, new ExpandedQuestionFragment(), "HomeFragment", R.id.fragment_container_home, true);
                            } else{
                                connectionErrorAlert.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            connectionErrorAlert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        connectionErrorAlert.show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("testo", testo);
                params.put("punteggio", ""+punteggioTotale);
                params.put("numeroValutazioni", ""+numeroValutazioni);
                params.put("data", data);
                params.put("email", email);
                params.put("idDomanda", ""+idDomanda);
                params.put("emailUserRicevente", domandaItem.getAutore());

                return params;
            }

        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }

    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();
        connection.close();
    }


}
