package com.example.edengardens.MainActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.edengardens.EvaluateActivity.EvaluateActivity;
import com.example.edengardens.HomeActivity.HomeActivity;
import com.example.edengardens.R;
import com.example.edengardens.Utilities;


public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(Utilities.KEY_LOGIN, MODE_PRIVATE);

        checkLogin();
    }

    @Override
    public void onBackPressed() {
        FragmentManager mgr = getSupportFragmentManager();
        if (mgr.getBackStackEntryCount() == 0) {
            // non ci sono fragment nella coda
            super.onBackPressed();
        } else {
            mgr.popBackStack();
        }
    }

    private void checkLogin(){
        //recuperiamo l'username
        String username = sharedPreferences.getString(Utilities.KEY_USERNAME, "");//gli passiamo una stringa chiave e un valore di default
        String admin = sharedPreferences.getString(Utilities.KEY_ADMIN, "");

        //se la stringa prende il valore di default allora non è loggato
        if(!username.isEmpty()){
            Intent intent= new Intent(this, HomeActivity.class);
            startActivity(intent);
            finish();
        } else if(!admin.isEmpty()) {
            //admin
            Intent intent= new Intent(this, EvaluateActivity.class);
            startActivity(intent);
            finish();
        }else{
            Utilities.insertFragment(this, new LoginFragment(), "loginFragment", R.id.fragment_container, false);
        }
    }

}
