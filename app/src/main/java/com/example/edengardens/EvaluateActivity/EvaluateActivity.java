package com.example.edengardens.EvaluateActivity;

import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.edengardens.AddActivity.AddFragment;
import com.example.edengardens.R;
import com.example.edengardens.Utilities;

public class EvaluateActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluate);

        Utilities.insertFragment(this, new EvaluateFragment(), "evaluateFragment", R.id.evaluateLayoutActivity, false);
    }
}
