package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Plant.PlantCardItem;
import com.example.edengardens.Utilities;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

public class ShowPlantFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.plant_descriprion_fragment, container, false);

        final FragmentActivity activity = getActivity();
        if(activity != null) {
            Gson gson = new Gson();
            final SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_PIANTA_SELEZIONATA, Activity.MODE_PRIVATE);

            String json = sharedPreferences.getString(Utilities.KEY_PIANTA_SELEZIONATA, "");
            PlantCardItem card = gson.fromJson(json, PlantCardItem.class);

            ImageView imageView = view.findViewById(R.id.idPiantaImageGrande);

            Picasso.get()
                    .load(Utilities.IMAGE_PREFIX + card.getImmaginePath())
                    .fit()
                    .centerCrop()
                    .into(imageView);

            TextView nome = view.findViewById(R.id.idNomePianta);
            TextView descrizione = view.findViewById(R.id.idDescrizionePianta);

            nome.setText(card.getNome());
            String desc = card.getDescrizione();
            if (desc.equals("/")) {
                descrizione.setVisibility(View.GONE);
            } else {
                descrizione.setText(desc);
            }


            view.findViewById(R.id.visualizzaDomandeButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //switch fragment
                    HomeActivity homeActivity = (HomeActivity) getActivity();
                    if (homeActivity != null)
                        homeActivity.addIdInBackList(homeActivity.getLastItemInBackList());

                    Utilities.insertFragment(activity, new PlantQuestionsFragment(), "PlantQuestionsFragment", R.id.fragment_container_home, true);
                }
            });

        }
        return view;
    }
}
