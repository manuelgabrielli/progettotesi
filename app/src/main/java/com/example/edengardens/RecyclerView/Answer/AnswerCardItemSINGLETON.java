package com.example.edengardens.RecyclerView.Answer;


import java.util.ArrayList;
import java.util.List;

public class AnswerCardItemSINGLETON {
    private List<AnswerCardItem> answerCardItemList =new ArrayList<>();
    private static final AnswerCardItemSINGLETON ourInstance = new AnswerCardItemSINGLETON();

    public static AnswerCardItemSINGLETON getInstance(){
        return ourInstance;
    }

    private AnswerCardItemSINGLETON(){

    }
    public void addCardItemRisposta(AnswerCardItem answerCardItem){

        this.answerCardItemList.add(0, answerCardItem);
    }

    public List<AnswerCardItem> getListRisposta(){
        return this.answerCardItemList;
    }

    public AnswerCardItem getCardRispostaItem(int position){
        return this.answerCardItemList.get(position);
    }

    public void emptyListRisposta(){
        this.answerCardItemList.clear();
    }
}
