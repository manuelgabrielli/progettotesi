package com.example.edengardens.HomeActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Question.QuestionCardItem;
import com.example.edengardens.RecyclerView.Question.QuestionCardItemSINGLETON;
import com.example.edengardens.RecyclerView.Plant.PlantCardItem;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import com.google.gson.Gson;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class PlantQuestionsFragment extends FollowFragment {

    private static final String TAG = "PLANT_QUESTION";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void createRequest(View view) {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = super.activity.getSharedPreferences(Utilities.KEY_PIANTA_SELEZIONATA, MODE_PRIVATE);
        String json = sharedPreferences.getString(Utilities.KEY_PIANTA_SELEZIONATA, "");
        String url = "https://ftp.edengardens.altervista.org/piante.php?pianta="+gson.fromJson(json, PlantCardItem.class).getId();;

        QuestionCardItemSINGLETON.getInstance().emptyPlantQuestions();

        progressBarLayout.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++){
                                int id=jsonArray.getJSONObject(i).getInt("idDomanda");
                                String titolo=jsonArray.getJSONObject(i).getString("titolo");
                                String domanda=jsonArray.getJSONObject(i).getString("testo");
                                String date=jsonArray.getJSONObject(i).getString("orario");
                                String fiore= jsonArray.getJSONObject(i).getString("nomePianta");
                                String autore=jsonArray.getJSONObject(i).getString("email");
                                String immagine=jsonArray.getJSONObject(i).getString("immagine_domanda");
                                QuestionCardItem questionCardItem;
                                if(!immagine.equals(Utilities.NULL)){
                                    questionCardItem = new QuestionCardItem(id,titolo,domanda,date,immagine,fiore,autore);
                                } else{
                                    questionCardItem = new QuestionCardItem(id,titolo,domanda,date,fiore,autore);
                                }
                                QuestionCardItemSINGLETON.getInstance().addPlantQuestion(questionCardItem);
                            }
                            progressBarLayout.setVisibility(View.GONE);
                            setRecyclerView(PlantQuestionsFragment.super.view, R.string.noDomandePianta, QuestionCardItemSINGLETON.getInstance().getListPlantQuestions());

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBarLayout.setVisibility(View.GONE);
                            AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                    .setTitle(R.string.errore)
                                    .setMessage(R.string.problema)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    }).create();
                            alert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){



        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }

    @Override
    public void onItemClick(int position) {
        SharedPreferences sharedPreferences = super.activity.getSharedPreferences(Utilities.KEY_DOMANDASELEZIONATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
        Gson gson = new Gson();
        String json = gson.toJson(QuestionCardItemSINGLETON.getInstance().getPlantQuestion(position));
        editor.putString(Utilities.KEY_DOMANDASELEZIONATA_SEARCH,json);
        editor.putString(Utilities.CATEGORIA_DOMANDA, Utilities.KEY_DOMANDASELEZIONATA_SEARCH);
        editor.apply(); //NECESSARIO

        //può essere chiamato o dalle domande seguite o dalle piante
        HomeActivity activity = (HomeActivity)getActivity();
        if(activity != null)
            activity.addIdInBackList(activity.getLastItemInBackList());

        Utilities.insertFragment(activity, new ExpandedQuestionFragment(), "ExpansiveQuestionFragment", R.id.fragment_container_home, true);
    }
}
