package com.example.edengardens.MainActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import se.simbio.encryption.Encryption;

public class RegistrationFragment extends Fragment {

    private static final String PASSWORD_VALIDATOR =  "^(?=.*[0-9])(?=.*[A-Z])(?=\\S+$).{8,50}$";
    private static final String TAG = "REGISTER_REQUEST";

    private String textNome;
    private String textCognome;
    private String textCitta;
    private String textEmail;
    private String textPassword;
    private String textRipetiPassword;

    private EditText nameEditText;
    private EditText cognomeEditText;
    private EditText cittaEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText ripetiPasswordEditText;

    private TextInputLayout emailLayout;
    private TextInputLayout passwordLayout;
    private TextInputLayout ripetiLayout;

    private Snackbar snackbar;
    private boolean error = false;
    private Connection connection;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        final View view=inflater.inflate(R.layout.registration_fragment,container,false);

        final Activity activity = getActivity();
        if(activity != null) {
            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container), activity, TAG);

            nameEditText = view.findViewById(R.id.idTitoloAggiunta);
            cognomeEditText = view.findViewById(R.id.idCognome);
            cittaEditText = view.findViewById(R.id.idCittaRegistrazione);
            emailEditText = view.findViewById(R.id.idEmailRegistrazione);
            passwordEditText = view.findViewById(R.id.idPasswordRegistrazione);
            ripetiPasswordEditText = view.findViewById(R.id.idRipetiPassword);

            emailLayout = view.findViewById(R.id.idEmailInputLayout);
            ripetiLayout = view.findViewById(R.id.idRipetiPasswordInputLayout);
            passwordLayout = view.findViewById(R.id.idPasswordInputLayout);

            //resetto l'errore della password
            passwordEditText.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (passwordLayout.getError() != null) {
                        passwordLayout.setErrorEnabled(false);
                        passwordLayout.setHelperTextEnabled(true);
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            Button button = view.findViewById(R.id.idButtonregistrazione);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    error = false;
                    update(); //aggiorno i campi

                    //faccio i controlli
                    if (textNome.length() == 0 || textCognome.length() == 0 || textCitta.length() == 0 ||
                            textEmail.length() == 0 || textPassword.length() == 0 || textRipetiPassword.length() == 0) {
                        snackbar = Snackbar.make(activity.findViewById(R.id.fragment_container), R.string.compila_campi, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok, (new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        }));
                        snackbar.show();
                    } else {
                        //resetto gli errori
                        emailLayout.setErrorEnabled(false);
                        if (passwordLayout.getError() != null) {
                            passwordLayout.setErrorEnabled(false);
                            passwordLayout.setHelperTextEnabled(true);
                        }
                        ripetiLayout.setErrorEnabled(false);

                        if (!isEmailValid(textEmail)) {
                            emailLayout.setError(getResources().getString(R.string.mailInvalida));
                            error = true;
                        }
                        if (!textPassword.equals(textRipetiPassword)) {
                            ripetiLayout.setError(getResources().getString(R.string.passwordDiverse));
                            error = true;
                        }
                        if (!isPasswordValid(textPassword)) {
                            passwordLayout.setError(getResources().getString(R.string.passwordInvalida));
                            error = true;
                        }
                        if (!error) {
                            //i parametri sono tutti giusti
                            if (connection.isConnected()) {
                                createRequest();
                            } else {
                                Log.e(TAG, "onClick: no connection");
                                connection.showSnackbar();
                            }
                        }
                    }

                }
            });
        }
        return view;
    }

    private void update() {
        textNome = nameEditText.getText().toString();
        textCognome = cognomeEditText.getText().toString();
        textCitta = cittaEditText.getText().toString();
        textEmail = emailEditText.getText().toString();
        textPassword = passwordEditText.getText().toString();
        textRipetiPassword = ripetiPasswordEditText.getText().toString();
    }

    private boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(String password){
        //almeno un numero, essere lunga almeno 8 e max 50, contenere una lettera Maiuscola, non può contenere spazi
        return password.matches( PASSWORD_VALIDATOR );
    }

    private void createRequest(){
        String url = "https://ftp.edengardens.altervista.org/registrazione.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            //registrazione andata a buon fine
                            //se il risultato è false vuol dire che c'è già un utente con quella mail
                            if(!jsonObject.getBoolean("result")){
                                AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                        .setTitle(R.string.errore)
                                        .setMessage(R.string.emailUtilizzata)
                                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                            }
                                        }).create();
                                alert.show();
                            } else {
                                AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                        .setTitle(R.string.congratulazioni)
                                        .setMessage(R.string.successo_registrazione_utente)
                                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //switch fragment
                                                Utilities.insertFragment(getActivity(), new LoginFragment(), "loginFragment", R.id.fragment_container, false);
                                            }
                                        }).create();
                                alert.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                    .setTitle(R.string.errore)
                                    .setMessage(R.string.problema)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    }).create();
                            alert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();

                params.put("email",textEmail);
                params.put("nome", textNome);
                params.put("cognome", textCognome);
                params.put("citta", textCitta);

                //cripto la password
                byte[] iv = new byte[16];
                Encryption encryption = Encryption.getDefault(Utilities.getKey(), Utilities.getSalt(), iv);
                String encrypted = encryption.encryptOrNull(textPassword);
                if(encrypted.contains("\n")){
                    encrypted = encrypted.replace("\n", "");
                }
                params.put("password",encrypted);

                return params;
            }

        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }

    @Override
    public void onStart() {
        super.onStart();
        //registerNetworkCallback();
        connection.registerNetworkCallback();
    }

    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo

        connection.close();
    }
}
