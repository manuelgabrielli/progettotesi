package com.example.edengardens.AddActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.HomeActivity.PhotoFragment;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Question.QuestionCardItem;
import com.example.edengardens.RecyclerView.Question.QuestionCardItemSINGLETON;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;


public class AddFragment extends PhotoFragment {
    private static final String TAG = "ADD_QUESTION_REQUEST";

    private Activity activity;
    private Spinner categorieSpinner;
    private Spinner pianteSpinner;
    private List<Integer> idCategorie;
    private List<Integer> idPiante;

    private Snackbar snackbar;

    private String textTitolo;
    private String textDomanda;

    private EditText titoloEditText;
    private EditText domandaEditText;

    private Connection connection;

    private AlertDialog connectionErrorAlert;

    private String username;

    private ConstraintLayout progressBarLayout;

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.add_fragment, container, false);

        String s = Utilities.getTime();
        activity = getActivity();

        if(activity != null) {
            username = activity.getSharedPreferences(Utilities.KEY_LOGIN, Context.MODE_PRIVATE).getString(Utilities.KEY_USERNAME, "");


            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_add), activity, TAG);

            titoloEditText = view.findViewById(R.id.idTitoloAggiunta);
            domandaEditText = view.findViewById(R.id.idDomandaAggiunta);

            progressBarLayout = view.findViewById(R.id.progressBarLayout);

            final ImageButton deleteImage = view.findViewById(R.id.deleteImageButton);
            final ImageView imageView = view.findViewById(R.id.addImageView);
            final Button captureButton = view.findViewById(R.id.captureButton);

            deleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setImageName(null);
                    imageView.setImageResource(android.R.color.transparent);
                    deleteImage.setVisibility(View.GONE);
                    captureButton.setText(R.string.inserisci_una_foto);
                }
            });

            final AlertDialog choseImageDialog =  new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.inserisci_una_foto)
                    .setItems(R.array.change_image_question_array, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item
                            switch (which){
                                case 0: dispatchPictureIntent(activity, Utilities.REQUEST_IMAGE_CAPTURE);
                                    break;
                                case 1: dispatchPictureIntent(activity, Utilities.PICK_FROM_GALLERY);
                                    break;
                            }
                        }
                    }).create();

            captureButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    choseImageDialog.show();
                }
            });

            //gestisco il click sul pulsante aggiungi
            view.findViewById(R.id.fab_add_aggiungi).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (connection.isConnected()) {
                        update();

                        if (textDomanda.length() == 0 || textTitolo.length() == 0) {
                            snackbar = Snackbar.make(activity.findViewById(R.id.fragment_container_add), R.string.compila_campi, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok, (new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    snackbar.dismiss();
                                }
                            }));
                            snackbar.show();
                        } else {
                            if (getImageName() == null) {
                                //se la foto non è stata scattata
                                progressBarLayout.setVisibility(View.VISIBLE);
                                createQuestionWithoutImageRequest();
                            } else {
                                uploadImage();
                            }
                        }
                    } else {
                        Log.e(TAG, "onClick: no connection");
                        connection.showSnackbar();
                    }

                }
            });

            //creo il messaggio di errore da visualizzare nel caso qualcosa andasse storto
            connectionErrorAlert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.errore_connessione)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            pianteSpinner = view.findViewById(R.id.idPiantaSpinner);

            //gestisco lo spinner delle categorie
            categorieSpinner = view.findViewById(R.id.idCategoriaSpinner);
            getCategoriesRequest();
            //aggiungo listener a categorie spinner per cambiare quelle delle piante se viene selezioanto un elemento
            categorieSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    getPlantsRequest(idCategorie.get(categorieSpinner.getSelectedItemPosition()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }
            });

        }
        return view;
    }

    private void update() {
        textDomanda = domandaEditText.getText().toString();
        textTitolo = titoloEditText.getText().toString();
    }


    private void getCategoriesRequest(){
        String url = "https://ftp.edengardens.altervista.org/categorie.php";

        //creiamo richiesta di tipo json con un array contenente almeno 1 elemento
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                try {
                    idCategorie = new ArrayList<>();

                    List<String> categories = new ArrayList<>();

                    for (int i=0; i<response.length(); i++)
                    {
                        idCategorie.add(response.getJSONObject(i).getInt("idCategoria"));
                        categories.add(response.getJSONObject(i).getString("nome"));
                    }

                    ArrayAdapter<String> adapterCategorie = new ArrayAdapter<>(Objects.requireNonNull(getContext()), R.layout.list, categories);
                    adapterCategorie.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    categorieSpinner.setAdapter(adapterCategorie);

                } catch (JSONException e) {
                    e.printStackTrace();
                    //c'è stato qualche problema
                    connectionErrorAlert.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                Log.e(TAG, error.toString());
            }
        });

        //bisogna assegnarli un tag e inserirlo nella coda
        jsonArrayRequest.setTag(TAG);
        //requestQueue.add(jsonArrayRequest);
        connection.addRequest(jsonArrayRequest);

    }

    private void getPlantsRequest(int idCategoria){
        String url = "https://ftp.edengardens.altervista.org/piante.php?categoria=" + idCategoria;

        //creiamo richiesta di tipo json con un array contenente almeno 1 elemento
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                try {
                    idPiante = new ArrayList<>();

                    List<String> piante = new ArrayList<>();

                    for (int i=0; i<response.length(); i++)
                    {
                        idPiante.add(response.getJSONObject(i).getInt("idPianta"));
                        piante.add(response.getJSONObject(i).getString("nome"));
                    }

                    ArrayAdapter<String> adapterCategorie = new ArrayAdapter<>(Objects.requireNonNull(getContext()), R.layout.list, piante);
                    adapterCategorie.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    pianteSpinner.setAdapter(adapterCategorie);

                } catch (JSONException e) {
                    e.printStackTrace();
                    //c'è stato qualche problema
                    connectionErrorAlert.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                Log.e(TAG, error.toString());
            }
        });

        //bisogna assegnarli un tag e inserirlo nella coda
        jsonArrayRequest.setTag(TAG);

        connection.addRequest(jsonArrayRequest);

    }

    private void createQuestionWithImageRequest(){
        final String imageName = getImageName();
        final String pianta = "" + idPiante.get(pianteSpinner.getSelectedItemPosition());
        final String time = Utilities.getTime();

        String url = "https://ftp.edengardens.altervista.org/domanda.php?titolo="+textTitolo+"&domanda="+textDomanda+"&pianta="+pianta+"&immagine="+imageName+"&orario="+time+"&email="+username;

        //creiamo richiesta di tipo json con un array contenente almeno 1 elemento
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                try {
                    JSONObject jsonObject = response.getJSONObject(0);

                    progressBarLayout.setVisibility(View.GONE);

                    //registrazione domanda andata a buon fine
                    if(!jsonObject.getBoolean("result")){
                        connectionErrorAlert.show();
                    } else {
                        //aggiungo la card al singleton
                        QuestionCardItemSINGLETON.getInstance().addCardItemDomandaOnTop(new QuestionCardItem(jsonObject.getInt("id"),
                                textTitolo, textDomanda, time, imageName, pianteSpinner.getSelectedItem().toString(), username));

                        AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                .setTitle(R.string.congratulazioni)
                                .setMessage(R.string.successo_registrazione_domanda)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //finisco l'activity
                                        activity.setResult(RESULT_OK);
                                        activity.finish();
                                    }
                                }).create();
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //c'è stato qualche problema
                    connectionErrorAlert.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                Log.e(TAG, error.toString());
            }
        });

        //bisogna assegnarli un tag e inserirlo nella coda
        jsonArrayRequest.setTag(TAG);
        //requestQueue.add(jsonArrayRequest);
        connection.addRequest(jsonArrayRequest);

    }

    private void createQuestionWithoutImageRequest(){
        final String pianta = "" + idPiante.get(pianteSpinner.getSelectedItemPosition());
        final String time = Utilities.getTime();
        String url = "https://ftp.edengardens.altervista.org/domanda.php?titolo="+textTitolo+"&domanda="+textDomanda+"&pianta="+pianta+"&orario="+time+"&email="+username;

        //creiamo richiesta di tipo json con un array contenente almeno 1 elemento
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { //listener che gestisce l'ottenimento dei dati
                try {
                    JSONObject jsonObject = response.getJSONObject(0);

                    progressBarLayout.setVisibility(View.GONE);

                    //registrazione domanda andata a buon fine
                    if(!jsonObject.getBoolean("result")){
                        connectionErrorAlert.show();
                    } else {
                        //aggiungo la card al singleton
                        QuestionCardItemSINGLETON.getInstance().addCardItemDomandaOnTop(new QuestionCardItem(jsonObject.getInt("id"),
                                textTitolo, textDomanda, time, pianteSpinner.getSelectedItem().toString(), username));

                        AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                .setTitle(R.string.congratulazioni)
                                .setMessage(R.string.successo_registrazione_domanda)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //finisco l'activity
                                        activity.setResult(RESULT_OK);
                                        activity.finish();
                                    }
                                }).create();
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //c'è stato qualche problema
                    connectionErrorAlert.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { //listener che gestisce l'errore
                Log.e(TAG, error.toString());
            }
        });

        //bisogna assegnarli un tag e inserirlo nella coda
        jsonArrayRequest.setTag(TAG);
        //requestQueue.add(jsonArrayRequest);
        connection.addRequest(jsonArrayRequest);

    }

    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }

    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo

        connection.close();
    }

    private void uploadImage() {
        final String imageName = getImageName();
        Bitmap imageBitmap = getImageBitmap();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY, byteArrayOutputStream);

        //rappresentazione in stringa dell'immagine
        final String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

        String url = "https://ftp.edengardens.altervista.org/savePicture.php";

        progressBarLayout.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            //qualcosa è andato storto
                            if(!jsonObject.getBoolean("result")){
                                connectionErrorAlert.show();
                            } else{
                                createQuestionWithImageRequest();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("nome",imageName);
                params.put("immagine", encodedImage);

                return params;
            }
        };
        //bisogna assegnarli un tag e inserirlo nella coda
       stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }

    /**
     * Funzione chiamata dopo che una fotografia viene fatta per salvarla nella galleria
     * e visualizzarla
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utilities.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Uri currentPhotoUri = getPhotoUri();
            if(currentPhotoUri != null){
                setImageBitmap(Utilities.getImageBitmap(Objects.requireNonNull(getActivity()), currentPhotoUri));
                Bitmap bitmap = getImageBitmap();
                if (bitmap!= null){
                    try {
                        saveImage(bitmap);

                        //visualizzo l'immagine
                        ImageView imageView = view.findViewById(R.id.addImageView);
                        imageView.setImageBitmap(bitmap);

                        ImageButton deleteImage = view.findViewById(R.id.deleteImageButton);
                        deleteImage.setVisibility(View.VISIBLE);

                        Button captureButton = view.findViewById(R.id.captureButton);
                        captureButton.setText(R.string.cambia_foto);
                    } catch (IOException e) {
                        setImageName(null);
                        e.printStackTrace();
                    }


                }
            }
        }

        if (requestCode == Utilities.PICK_FROM_GALLERY && resultCode == RESULT_OK){
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = Objects.requireNonNull(getContext()).getContentResolver().openInputStream(imageUri);
                setImageBitmap(BitmapFactory.decodeStream(imageStream));

                //creo nome immagine
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                setImageName("JPEG_" + timeStamp + ".jpg");

                //visualizzo l'immagine

                ImageView imageView = view.findViewById(R.id.addImageView);
                imageView.setImageBitmap(getImageBitmap());

                ImageButton deleteImage = view.findViewById(R.id.deleteImageButton);
                deleteImage.setVisibility(View.VISIBLE);


                Button captureButton = view.findViewById(R.id.captureButton);
                captureButton.setText(R.string.cambia_foto);
            } catch (FileNotFoundException e) {
                setImageName(null);
                e.printStackTrace();
            }
        }
    }
}
