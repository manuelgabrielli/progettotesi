package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Category.CategoryCardAdapter;
import com.example.edengardens.RecyclerView.Category.CategoryCardItem;
import com.example.edengardens.RecyclerView.Category.CategoryCardItemSINGLETON;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Objects;

public class SearchCategoryFragment extends Fragment implements CategoryCardAdapter.OnItemListener{
    private static final String LOG = "Search-Category-Fragment_LAB";

    private CategoryCardAdapter adapter;

    private static final String TAG = "CATEGORY_REQUEST";
    private Connection connection;

    private ConstraintLayout progressBarLayout;

    private View view;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.search_categories_fragment, container, false);

        activity = getActivity();
        if(activity != null) {

            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);

            SearchView searchView = view.findViewById(R.id.idSearchCategoria);

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    return true;
                }
            });
            //riscrivo la netwrokcallback della connection in modo che ogni volta che sente internet ricarica la pagina
            connection.setNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    connection.setNetworkConnected(true);
                    connection.dismissSnackbar();
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() { //questo metodo serve per far girare il codice sul thread principale, perchè solo li posso cambaire la view
                            createRequest();
                        }
                    });

                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    connection.setNetworkConnected(false);
                    connection.showSnackbar();
                }

            });


            progressBarLayout = view.findViewById(R.id.progressBarCategoriaLayout);

            connection.showSnackbar(); //faccio vidualizzare di default la snackbar che non c'è connessione

        }
        return view;
    }

    private void setRecyclerView(View view){
        // Set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.categoriaRecyclerView);


        adapter = new CategoryCardAdapter(CategoryCardItemSINGLETON.getInstance().getListCategoria(), this);
        recyclerView.setAdapter(adapter);
    }

    private void createRequest(){

        String url = "https://ftp.edengardens.altervista.org/categorie.php";


        progressBarLayout.setVisibility(View.VISIBLE);

        CategoryCardItemSINGLETON.getInstance().emptyList();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++){
                                int id=jsonArray.getJSONObject(i).getInt("idCategoria");
                                String nome=jsonArray.getJSONObject(i).getString("nome");
                                CategoryCardItem categoryCardItem =new CategoryCardItem(id,nome);
                                CategoryCardItemSINGLETON.getInstance().addCardItemCategoria(categoryCardItem);
                            }
                            progressBarLayout.setVisibility(View.GONE);

                            setRecyclerView(view);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBarLayout.setVisibility(View.GONE);
                            AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                    .setTitle(R.string.errore)
                                    .setMessage(R.string.problema)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    }).create();
                            alert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){



        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }

    @Override
    public void onStart() {
        super.onStart();
        //registerNetworkCallback();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo

        connection.close();
    }

    @Override
    public void onItemClick(int position) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_CATEGORIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
        editor.putInt(Utilities.KEY_CATEGORIA, CategoryCardItemSINGLETON.getInstance().getCardCategoriaItem(position).getId());
        editor.apply(); //NECESSARIO

        HomeActivity homeActivity = (HomeActivity) getActivity();
        if (homeActivity != null){
            homeActivity.addIdInBackList(homeActivity.getLastItemInBackList());
        }

        Utilities.insertFragment((FragmentActivity) activity, new SearchPlantsFragment(), "SearchPlantsFragment", R.id.fragment_container_home,true);
    }
}
