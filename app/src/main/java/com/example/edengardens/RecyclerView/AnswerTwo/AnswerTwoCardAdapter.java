package com.example.edengardens.RecyclerView.AnswerTwo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edengardens.R;

import java.util.ArrayList;
import java.util.List;

public class AnswerTwoCardAdapter extends RecyclerView.Adapter<AnswerTwoCardAdapter.ListViewHolder> {

    //list that contains ALL the element added by the user
    private List<AnswerTwoCardItem> cardItemList;
    private OnItemListener listener;

    public AnswerTwoCardAdapter(List<AnswerTwoCardItem> cardItemList, OnItemListener listener) {
        this.cardItemList = new ArrayList<>(cardItemList);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.answer2_layout, parent, false);
        return new ListViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        AnswerTwoCardItem currentListItem = cardItemList.get(position);
        holder.risposta.setText(currentListItem.getRisposta());
        holder.autore.setText(currentListItem.getAutore());
        holder.data.setText(currentListItem.getData());
    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }

    static class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView risposta;
        TextView autore;
        TextView data;
        OnItemListener itemListener;


        ListViewHolder(@NonNull View itemView, OnItemListener lister) {
            super(itemView);
            risposta = itemView.findViewById(R.id.idRisposta2);
            autore = itemView.findViewById(R.id.idAutoreRisposta2);
            data = itemView.findViewById(R.id.idDataRisposta2);

            itemListener = lister;
            itemView.setOnClickListener(this);

           autore.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   itemListener.onAuthorNameClick(getAdapterPosition());
               }
           });
        }

        @Override
        public void onClick(View view) {
            //il click sull'elemento generale non deve fare nulla
        }
    }

    public void updateList() {
        cardItemList.clear();
        cardItemList.addAll(AnswerTwoCardItemSINGLETON.getInstance().getList());
        notifyDataSetChanged();
    }

    public interface OnItemListener{
        void onAuthorNameClick(int position);
    }
}
