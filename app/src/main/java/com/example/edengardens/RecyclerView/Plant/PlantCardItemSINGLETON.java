package com.example.edengardens.RecyclerView.Plant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlantCardItemSINGLETON {
    private List<PlantCardItem> cardItemCategoriaList =new ArrayList<>();
    private static final PlantCardItemSINGLETON ourInstance = new PlantCardItemSINGLETON();

    public static PlantCardItemSINGLETON getInstance(){
        return ourInstance;
    }

    private PlantCardItemSINGLETON(){ }

    public void setList(List<PlantCardItem> list){
        this.cardItemCategoriaList = new ArrayList<>(list);
    }

    public void addCardItemPianta(PlantCardItem cardItemCategoria){
        this.cardItemCategoriaList.add(cardItemCategoria);
    }

    public List<PlantCardItem> getListPianta(){
        return cardItemCategoriaList;
    }

    public PlantCardItem getCardPiantaItem(int position){
        return cardItemCategoriaList.get(position);
    }

    public void emptyList(){
        cardItemCategoriaList.clear();
    }
}
