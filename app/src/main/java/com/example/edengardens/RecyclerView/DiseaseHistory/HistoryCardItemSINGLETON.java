package com.example.edengardens.RecyclerView.DiseaseHistory;

import java.util.ArrayList;
import java.util.List;

public class HistoryCardItemSINGLETON {
    private List<HistoryCardItem> cardItemMalattieList =new ArrayList<>();
    private static final HistoryCardItemSINGLETON ourInstance = new HistoryCardItemSINGLETON();


    public static HistoryCardItemSINGLETON getInstance(){
        return ourInstance;
    }

    private HistoryCardItemSINGLETON(){
    }

    public void addCardItemMalattia(HistoryCardItem cardItemMalattia){
        this.cardItemMalattieList.add(cardItemMalattia);
    }

    public List<HistoryCardItem> getMalattieList(){return this.cardItemMalattieList;}

    public HistoryCardItem getCardMalattieItem(int position){
        return cardItemMalattieList.get(position);
    }

    public void emptyList(){this.cardItemMalattieList.clear();}

    public void deleteMalattia(int position){
        cardItemMalattieList.remove(position);
    }

}
