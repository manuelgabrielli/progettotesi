package com.example.edengardens.RecyclerView.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoryCardItemSINGLETON {
    private List<CategoryCardItem> categoryCardItemList =new ArrayList<>();
    private static final CategoryCardItemSINGLETON ourInstance = new CategoryCardItemSINGLETON();

    public static CategoryCardItemSINGLETON getInstance(){
        return ourInstance;
    }

    private CategoryCardItemSINGLETON(){ }

    public void addCardItemCategoria(CategoryCardItem categoryCardItem){
        this.categoryCardItemList.add(categoryCardItem);
    }

    public void setList(List<CategoryCardItem> list){
        this.categoryCardItemList = new ArrayList<>(list);
    }

    public List<CategoryCardItem> getListCategoria(){
        return categoryCardItemList;
    }

    public CategoryCardItem getCardCategoriaItem(int position){
        return categoryCardItemList.get(position);
    }

    public void emptyList(){
        categoryCardItemList.clear();
    }
}
