package com.example.edengardens.HomeActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.DiseaseHistory.HistoryCardItem;
import com.example.edengardens.RecyclerView.DiseaseHistory.HistoryCardItemSINGLETON;
import com.example.edengardens.RecyclerView.Question.QuestionCardItemSINGLETON;
import com.example.edengardens.RecyclerView.Notification.NotificationCardAdapter;
import com.example.edengardens.RecyclerView.Notification.NotificationCardItem;
import com.example.edengardens.RecyclerView.Notification.NotificationCardItemSINGLETON;
import com.example.edengardens.RecyclerView.Answer.AnswerCardItem;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.tensorflow.lite.support.common.FileUtil;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class NotificationFragment extends Fragment implements NotificationCardAdapter.OnItemListener, PopupMenu.OnMenuItemClickListener {

    private static final String TAG = "NOTIFICATION_REQUEST";
    private static final int INDICE_NO_MALATTIA = 0;
    private Connection connection;
    private ConstraintLayout noNotificheLayout;
    private ConstraintLayout progressBarLayout;
    private FragmentActivity activity;
    private String email;
    private AlertDialog alertDialog;
    private NotificationCardAdapter adapter;
    private int positionDelete;

    private Boolean asked = false; //questo mi serve per verificare se ho già chiesto al server le notifiche

    private List<String> labels;
    private List<String> modelLabels;
    private String piantaSanaNome;
    private boolean modelloCaricato = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        final View view = inflater.inflate(R.layout.notification_fragment, container, false);

        activity = getActivity();

        if(activity != null){
            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);

            noNotificheLayout = view.findViewById(R.id.noNotificheLayout);
            progressBarLayout = view.findViewById(R.id.progressBarNotificheLayout);

            //svuoto la lista di notifiche
            NotificationCardItemSINGLETON.getInstance().emptyList();

            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, MODE_PRIVATE);
            email = sharedPreferences.getString(Utilities.KEY_USERNAME,"");

            alertDialog = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.problema)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            connection.setNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    connection.setNetworkConnected(true);
                    connection.dismissSnackbar();
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() { //questo metodo serve per far girare il codice sul thread principale, perchè solo li posso cambaire la view
                            askNotifications(view);
                        }
                    });

                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    connection.setNetworkConnected(false);
                    connection.showSnackbar();
                }

            });

            //leggo le labels per la traduzione dei nomi delle malattie
            String language = Locale.getDefault().getLanguage();
            try {
                //da togliere commento quando verrà inserita la lingua inglese
                /*
                switch (language){
                    case "it": labels = FileUtil.loadLabels(activity, "labelsIta.txt");
                        labels.add(0, getResources().getString(R.string.malattia_non_rilevabile));
                        piantaSanaNome = getResources().getString(R.string.pianta_sana);
                        break;
                    default: labels = FileUtil.loadLabels(activity, "labelsEng.txt"); //lingua inglese
                        labels.add(0, getResources().getString(R.string.malattia_non_rilevabile));
                        piantaSanaNome = getResources().getString(R.string.pianta_sana);
                }*/
                //solo con lingua italiana
                labels = FileUtil.loadLabels(activity, "labelsIta.txt");
                labels.add(INDICE_NO_MALATTIA, getResources().getString(R.string.malattia_non_rilevabile));
                piantaSanaNome = getResources().getString(R.string.pianta_sana);

                /*----*/
                modelLabels = FileUtil.loadLabels(activity, "labelsModel.txt");
                modelLabels.add(INDICE_NO_MALATTIA, Utilities.MALATTIA_NON_DISPONIBILE);
            }  catch (IOException e) {
                e.printStackTrace();
                modelloCaricato = false;
            }

            connection.showSnackbar();
        }

        return view;
    }

    private void askNotifications(final View view) {
        if(!asked){

            String url = "https://ftp.edengardens.altervista.org/notifiche.php?email="+email;

            progressBarLayout.setVisibility(View.VISIBLE);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonArray = new JSONArray(response);

                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    String idAnalisiPianta = obj.getString("idAnalisiPianta");
                                    String idRisposta = obj.getString("idRisposta");
                                    String idDomanda = obj.getString("idDomanda");

                                    String testo;
                                    if(idAnalisiPianta.equals("null")){ //la notifica non rigurada una malattia
                                        if(idRisposta.equals("null")){
                                            testo = obj.getString("nome") + " " + obj.getString("cognome") + " " +getString(R.string.testo_notifica_risposta_uno);
                                        } else{
                                            testo = obj.getString("nome") + " " + obj.getString("cognome") + " " +getString(R.string.testo_notifica_risposta_due);
                                        }
                                    } else{
                                        testo = getString(R.string.testo_notifica_analisi_pianta);
                                    }

                                    String date = obj.getString("orario");
                                    String autore= obj.getString("emailUserMittente");
                                    String immagine = obj.getString("immagine_profilo");


                                    NotificationCardItem cardItemNotifica;
                                    if(!immagine.equals(Utilities.NULL)){
                                        cardItemNotifica = new NotificationCardItem(autore, testo, date, immagine, idDomanda, idRisposta, idAnalisiPianta);
                                    } else{
                                        cardItemNotifica = new NotificationCardItem(autore, testo, date, idDomanda, idRisposta, idAnalisiPianta);
                                    }
                                    NotificationCardItemSINGLETON.getInstance().addCardItemNotifica(cardItemNotifica);
                                }

                                progressBarLayout.setVisibility(View.GONE);

                                asked = true;//setto true perchè vado a chiedere le notifiche

                                setRecyclerView(view);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressBarLayout.setVisibility(View.GONE);
                                alertDialog.show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(TAG, error.toString());
                            alertDialog.show();
                        }
                    }){
            };

            //bisogna assegnarli un tag e inserirlo nella coda
            stringRequest.setTag(TAG);

            connection.addRequest(stringRequest);

        }
    }

    private void setRecyclerView(View view){
        // Set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.idNotificheRecyclerView);

        adapter = new NotificationCardAdapter(NotificationCardItemSINGLETON.getInstance().getNotificheList(), this);
        recyclerView.setAdapter(adapter);

        if(NotificationCardItemSINGLETON.getInstance().getNotificheList().isEmpty()){
            //faccio vedere il layout della scritta
            noNotificheLayout.setVisibility(View.VISIBLE);
        } else {
            noNotificheLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(int position) {
        NotificationCardItem notifica = NotificationCardItemSINGLETON.getInstance().getCardNotificaItem(position);

        if (notifica.isRispostaPresent() && notifica.isDomandaPresent()){
            //salvo la domanda
            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_DOMANDASELEZIONATA, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
            Gson gson = new Gson();
            String json = gson.toJson(QuestionCardItemSINGLETON.getInstance().getDomandaInList(notifica.getIdDomanda()));
            editor.putString(Utilities.KEY_DOMANDASELEZIONATA_HOME,json);
            editor.putString(Utilities.CATEGORIA_DOMANDA, Utilities.KEY_DOMANDASELEZIONATA_HOME);
            editor.apply(); //NECESSARIO
            //chiedo la risposta
            getAnswer(notifica.getIdRisposta());

        } else if(notifica.isDomandaPresent()){
            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_DOMANDASELEZIONATA, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
            Gson gson = new Gson();
            String json = gson.toJson(QuestionCardItemSINGLETON.getInstance().getDomandaInList(notifica.getIdDomanda()));
            editor.putString(Utilities.KEY_DOMANDASELEZIONATA_HOME,json);
            editor.putString(Utilities.CATEGORIA_DOMANDA, Utilities.KEY_DOMANDASELEZIONATA_HOME);
            editor.apply(); //NECESSARIO

            HomeActivity activity = (HomeActivity)getActivity();
            if(activity != null)
                activity.addIdInBackList(activity.getLastItemInBackList());

            Utilities.insertFragment(activity, new ExpandedQuestionFragment(), "ExpandedQuestionFragment", R.id.fragment_container_home, true);
        } else if(notifica.isAnalisiPiantaPresent()){
            //chiedo le info sull'analisi della malattia
            if(modelloCaricato){
                getAnalisiInfo(notifica);
            } else{
                alertDialog.show();
            }

        }
    }

    private void getAnalisiInfo(final NotificationCardItem notifica) {
        final String immagine = notifica.getIdAnalisiPianta();
        String url = "https://ftp.edengardens.altervista.org/analisiPiante.php?email="+email+"&idAnalisi="+immagine;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            JSONObject obj = jsonArray.getJSONObject(0);

                            String malattiaArchivio = obj.getString("nome_malattia");
                            boolean validata = obj.getInt("validata") != 0;
                            boolean corretta = obj.getInt("corretta") != 0;
                            boolean nuovaCategoria = obj.getInt("nuova_categoria") != 0;

                            String m;
                            if(nuovaCategoria){
                                m= labels.get(INDICE_NO_MALATTIA);
                            } else {
                                m = labels.get(modelLabels.indexOf(malattiaArchivio)); //prendo il nome della malattia tradotto
                            }
                            HistoryCardItem historyCardItem;
                            try {
                                String[] parts = m.split(Utilities.LABELS_SPLITTER);
                                String pianta = parts[0];
                                String disease = parts[1];

                                if(malattiaArchivio.contains(Utilities.MODEL_LABEL_HEALTHY)){
                                    disease = piantaSanaNome;
                                }
                                historyCardItem = new HistoryCardItem(malattiaArchivio, immagine, corretta, validata, pianta, disease);
                            } catch (Exception e){
                                //malattia non rilevabile
                                historyCardItem = new HistoryCardItem(malattiaArchivio, immagine, corretta, validata, getString(R.string.nessuna), m);
                            }

                            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_HISTORY, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
                            Gson gson = new Gson();
                            String json = gson.toJson(historyCardItem);
                            editor.putString(Utilities.KEY_HISTORY_ELEMENT,json);
                            editor.apply(); //NECESSARIO

                            HomeActivity activity = (HomeActivity)getActivity();
                            if(activity != null)
                                activity.addIdInBackList(activity.getLastItemInBackList());

                            Utilities.insertFragment(activity, new DiseaseHistoryElementFragment(), "DiseaseHistoryElementFragment", R.id.fragment_container_home, true);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alertDialog.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        alertDialog.show();
                    }
                }){
        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }

    private void getAnswer(final int idRisposta) {
        String url = "https://ftp.edengardens.altervista.org/notifiche.php?risposta="+idRisposta;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            JSONObject obj = jsonArray.getJSONObject(0);

                            String testo=obj.getString("testoRisposta");
                            String date=obj.getString("dataRisposta");
                            String autore=obj.getString("email");
                            double valutazione;
                            if(obj.getDouble("numeroValutazioni")==0)
                                valutazione=0;
                            else
                                valutazione=obj.getDouble("punteggioTotale")/obj.getDouble("numeroValutazioni");
                            AnswerCardItem risposta=new AnswerCardItem(idRisposta,autore,testo,date,(float)valutazione);

                            //salvo la rispsota e faccio partire l'intent
                            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_RISPOSTA1SELEZIONATA, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
                            Gson gson2 = new Gson();
                            String json = gson2.toJson(risposta);
                            editor.putString(Utilities.KEY_RISPOSTA1SELEZIONATA,json);
                            editor.apply(); //NECESSARIO

                            HomeActivity activity = (HomeActivity)getActivity();
                            if(activity != null)
                                activity.addIdInBackList(activity.getLastItemInBackList());

                            Utilities.insertFragment(activity, new AnswerTwoFragment(), "AnswerTwoFragment", R.id.fragment_container_home, true);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alertDialog.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        alertDialog.show();
                    }
                }){
        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }


    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo
        connection.close();
    }

    @Override
    public void onButtonDeleteNotificationClick(final int position, View v) {

        //click pulsante tre pallini per eliminare la notifica

        positionDelete = position;

        PopupMenu popupMenu = new PopupMenu(Objects.requireNonNull(getContext()), v);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.inflate(R.menu.notification_menu);
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.itemDeleteNotification) {
            deleteNotification();
            return true;
        }
        return false;
    }

    private void deleteNotification() {

        NotificationCardItem notifica = NotificationCardItemSINGLETON.getInstance().getCardNotificaItem(positionDelete);

        String url = "https://ftp.edengardens.altervista.org/notifiche.php?emailMittente="+notifica.getUserMittente()+"&emailDestinatario="+email+"&orario="+notifica.getData()+"&delete=true";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            JSONObject obj = jsonArray.getJSONObject(0);

                            if(obj.getBoolean("result")) {
                                NotificationCardItemSINGLETON.getInstance().deleteNotifica(positionDelete);
                                adapter.updateList();
                                if(NotificationCardItemSINGLETON.getInstance().getNotificheList().size() == 0){
                                    noNotificheLayout.setVisibility(View.VISIBLE);
                                }
                            } else{
                                alertDialog.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            alertDialog.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        alertDialog.show();
                    }
                }){
        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }
}
