package com.example.edengardens.RecyclerView.DiseaseHistory;

public class HistoryCardItem {
    private final String malattia;
    private String immaginePianta;
    private boolean corretta;
    private boolean valutata;
    private final String pianta;
    private final String disease;

    public HistoryCardItem(String malattia, String immaginePianta, boolean corretta, boolean valutata, String pianta, String disease) {
        this.malattia = malattia;
        this.immaginePianta = immaginePianta;
        this.corretta = corretta;
        this.valutata = valutata;
        this.pianta = pianta;
        this.disease = disease;
    }

    public String getMalattia() {
        return malattia;
    }

    public String getImmaginePianta() {
        return immaginePianta;
    }

    public boolean isCorretta() {
        return corretta;
    }

    public boolean isValutata() {
        return valutata;
    }

    public String getPianta() {
        return pianta;
    }

    public String getDisease() {
        return disease;
    }
}
