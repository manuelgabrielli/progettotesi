package com.example.edengardens;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import androidx.annotation.NonNull;


public class Connection {
    private RequestQueue requestQueue;

    //flag per controllare la connessione
    private boolean isNetworkConnected = false;

    private Snackbar snackbar;
    private Activity activity;
    private String tag;

    public Connection(Context context, View view, Activity activity, String tag) {
        requestQueue = Volley.newRequestQueue(context);
        this.activity = activity;
        snackbar = Snackbar.make(view, R.string.noConnection, Snackbar.LENGTH_INDEFINITE).setAction(R.string.impostazioni, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //apriamo le impostazioni di rete dell'utente
                setSettingsIntent();
            }
        }); //l'ultimo è il setAction ovvero l'azione che viene assegnta la bottoncino
        this.tag = tag;
    }

    public void registerNetworkCallback(){
        //questo ci serve per verificare se cè connessione o meno
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager != null){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){ //controlliamo la versione di android
                connectivityManager.registerDefaultNetworkCallback(networkCallback);
            } else{
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo(); //nelle versioni nuove è deprecato
                isNetworkConnected = networkInfo != null && networkInfo.isConnected();
            }
        }else{
            isNetworkConnected = false;
        }
    }

    private ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback(){
        @Override
        public void onAvailable(@NonNull Network network) {
            super.onAvailable(network);
            isNetworkConnected = true;
            snackbar.dismiss();
        }

        @Override
        public void onLost(@NonNull Network network) {
            super.onLost(network);
            isNetworkConnected = false;
            snackbar.show();
        }
    };

    public void setNetworkCallback(ConnectivityManager.NetworkCallback networkCallback) {
        this.networkCallback = networkCallback;
    }

    private void setSettingsIntent() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_WIRELESS_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if(intent.resolveActivity(activity.getPackageManager()) != null){
            activity.startActivity(intent);
        }
    }

    public boolean isConnected(){
        return this.isNetworkConnected;
    }

    public void showSnackbar(){
        this.snackbar.show();
    }

    public void addRequest(Request request){
        requestQueue.add(request);
    }

    public void close(){
        if(requestQueue != null){
            requestQueue.cancelAll(tag);
        }

        //togliamo il controllo della connessione
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager != null){
            connectivityManager.unregisterNetworkCallback(networkCallback);
        }
    }


    //////
    public void dismissSnackbar(){
        this.snackbar.dismiss();
    }

    public void setNetworkConnected(boolean connected){
        this.isNetworkConnected = connected;
    }
}
