package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Question.QuestionCardAdapter;
import com.example.edengardens.RecyclerView.Question.QuestionCardItem;
import com.example.edengardens.RecyclerView.Question.QuestionCardItemSINGLETON;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class FollowFragment extends Fragment implements QuestionCardAdapter.OnItemListener  {

    private static final String LOG = "Follow-Fragment_LAB";

    private QuestionCardAdapter adapter;

    private static final String TAG = "FOLLOW_REQUEST";
    protected Connection connection;

    protected ConstraintLayout progressBarLayout;

    protected View view;

    private ConstraintLayout noDomandaLayout;

    protected Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.follow_fragment, container, false);

        activity = getActivity();
        if(activity != null) {
            SearchView searchView = view.findViewById(R.id.idSearchFollow);
            noDomandaLayout = view.findViewById(R.id.noDomandeFollowLayout);
            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    return true;
                }
            });

            connection.setNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    connection.setNetworkConnected(true);
                    connection.dismissSnackbar();
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() { //questo metodo serve per far girare il codice sul thread principale, perchè solo li posso cambaire la view
                            createRequest(view);
                        }
                    });

                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    connection.setNetworkConnected(false);
                    connection.showSnackbar();
                }

            });

            progressBarLayout = view.findViewById(R.id.progressBarFollowLayout);

            connection.showSnackbar(); //faccio vidualizzare di default la snackbar che non c'è connessione
        }
        return view;
    }

    void setRecyclerView(View view, int message, List<QuestionCardItem> list){
        // Set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.idRecyclerFollow);

        adapter = new QuestionCardAdapter(list, this, this);
        recyclerView.setAdapter(adapter);

        if(list.isEmpty()){
            //faccio vedere il layout della scritta

            TextView noDomandeText = view.findViewById(R.id.noDomandeFollowText);

            noDomandeText.setText(message);

            noDomandaLayout.setVisibility(View.VISIBLE);
        } else {
            noDomandaLayout.setVisibility(View.GONE);
        }
    }

    protected void createRequest(final View view){

        SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, MODE_PRIVATE);
        String e=sharedPreferences.getString(Utilities.KEY_USERNAME,"");
        String url = "https://ftp.edengardens.altervista.org/follow.php?email="+e;

        QuestionCardItemSINGLETON.getInstance().emptyFollowList();

        progressBarLayout.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++){
                                int id=jsonArray.getJSONObject(i).getInt("idDomanda");
                                String titolo=jsonArray.getJSONObject(i).getString("titolo");
                                String domanda=jsonArray.getJSONObject(i).getString("testo");
                                String date=jsonArray.getJSONObject(i).getString("orario");
                                String fiore= jsonArray.getJSONObject(i).getString("nomePianta");
                                String autore=jsonArray.getJSONObject(i).getString("email");
                                QuestionCardItem questionCardItem;
                                if(jsonArray.getJSONObject(i).has("immagine_domanda")){
                                    String immagine = jsonArray.getJSONObject(i).getString("immagine_domanda");
                                    if(!immagine.equals(Utilities.NULL)) {
                                        questionCardItem = new QuestionCardItem(id,titolo,domanda,date,immagine,fiore,autore);
                                    } else{
                                        questionCardItem = new QuestionCardItem(id,titolo,domanda,date,fiore,autore);
                                    }
                                } else{
                                    questionCardItem = new QuestionCardItem(id,titolo,domanda,date,fiore,autore);
                                }
                                QuestionCardItemSINGLETON.getInstance().addFollowCard(questionCardItem);
                            }
                            progressBarLayout.setVisibility(View.GONE);
                            setRecyclerView(view, R.string.noDomandeSeguite, QuestionCardItemSINGLETON.getInstance().getFollowList());

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBarLayout.setVisibility(View.GONE);
                            AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                    .setTitle(R.string.errore)
                                    .setMessage(R.string.problema)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    }).create();
                            alert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){

        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);

    }

    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo
        connection.close();
    }



    @Override
    public void onItemClick(int position) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_DOMANDASELEZIONATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
        Gson gson = new Gson();
        String json = gson.toJson(QuestionCardItemSINGLETON.getInstance().getFollowCard(position));
        editor.putString(Utilities.KEY_DOMANDASELEZIONATA_FOLLOW,json);
        editor.putString(Utilities.CATEGORIA_DOMANDA, Utilities.KEY_DOMANDASELEZIONATA_FOLLOW);
        editor.apply(); //NECESSARIO

        //può essere chiamato o dalle domande seguite o dalle piante
        HomeActivity activity = (HomeActivity)getActivity();
        if(activity != null)
            activity.addIdInBackList(activity.getLastItemInBackList());
        Utilities.insertFragment(activity, new ExpandedQuestionFragment(), "ExpansiveQuestionFragment", R.id.fragment_container_home, true);
    }
}
