package com.example.edengardens.HomeActivity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class PhotoFragment extends Fragment {
    protected static final int QUALITY = 60;
    private Uri photoUri;
    private Bitmap imageBitmap;
    private String imageName;
    private int codiceIntent;

    /**
     * Motodo chiamato per lanciare l'activity per scattare la foto
     */
    protected void dispatchPictureIntent(Activity activity, int codice) {
        if (ContextCompat.checkSelfPermission(
                getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                getContext(), Manifest.permission.ACCESS_MEDIA_LOCATION) ==
                PackageManager.PERMISSION_GRANTED)  {
            // You can use the API that requires the permission.
            if(codice == Utilities.REQUEST_IMAGE_CAPTURE) {
                takePhoto(activity);
            } else if(codice == Utilities.PICK_FROM_GALLERY){
                choosePhoto();
            }
        } else {
            int code = 0;
            codiceIntent = codice;
            // You can directly ask for the permission.
            requestPermissions(new String[] {  Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_MEDIA_LOCATION}, code);
        }

    }

    private void choosePhoto() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, Utilities.PICK_FROM_GALLERY);
    }

    private void takePhoto(Activity activity) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile;
            try {
                photoFile = createImageFile(activity);// Continue only if the File was successfully created
                if (photoFile != null) {
                    ///storage/emulated/0/Android/data/com.example.edengardens/files/Pictures/JPEG_20200501_175825_4857740767044905810.jpg
                    photoUri = FileProvider.getUriForFile(Objects.requireNonNull(getContext()), getContext().getPackageName()+".fileprovider", photoFile);

                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    startActivityForResult(takePictureIntent, Utilities.REQUEST_IMAGE_CAPTURE);
                }
            } catch (IOException ex) {
                // Error occurred while creating the File
            }

        }
    }

    private File createImageFile(Activity activity) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageName = "JPEG_" + timeStamp + ".jpg";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        String currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted. Continue the action or workflow
                // in your app.
                if(codiceIntent == Utilities.REQUEST_IMAGE_CAPTURE) {
                    takePhoto(Objects.requireNonNull(getActivity()));
                } else if(codiceIntent == Utilities.PICK_FROM_GALLERY){
                    choosePhoto();
                }
            }
        }
    }

    /**
     * Metodo chiamato per salvare l'immagine nella galleria
     */
    protected void saveImage(Bitmap bitmap) throws IOException {
        if(imageName == null){
            String timeStamp =
                    new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(new Date());
            imageName = "JPEG_" + timeStamp + "_.jpg";
        }

        ContentResolver resolver = Objects.requireNonNull(getContext()).getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, imageName + ".jpg");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
        Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        OutputStream fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));

        //setta la qualità dell'immagine
        bitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY, fos);
        if (fos != null) {
            fos.close();
        }
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
