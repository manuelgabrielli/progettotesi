package com.example.edengardens.RecyclerView.Question;

import com.hadisatrio.optional.Optional;

public class QuestionCardItem {
    private int id;
    private String titolo;
    private String domanda;
    private String date;
    private String fiore;
    private String autore;

    private Optional<String> immagine = Optional.absent();

    public QuestionCardItem(int id, String titolo, String domanda, String date, String fiore, String autore){
        this.id=id;
        this.titolo=titolo;
        this.domanda=domanda;
        this.date=date;
        this.fiore=fiore;
        this.autore=autore;
    }

    public QuestionCardItem(int id, String titolo, String domanda, String date, String immagine, String fiore, String autore){
        this.id=id;
        this.titolo=titolo;
        this.domanda=domanda;
        this.date=date;
        if(!immagine.equals("null")) {
            this.immagine = Optional.of(immagine);
        }
        this.fiore=fiore;
        this.autore=autore;
    }


    public int getId() {
        return id;
    }

    public String getAutore() {
        return autore;
    }

    public String getDomanda() {
        return domanda;
    }

    public String getTitolo() {
        return titolo;
    }

    public String getDate() {
        return date;
    }

    public String getFiore() {
        return fiore;
    }

    public boolean isimagePresent(){
        return immagine.isPresent();
    }

    public String getImage(){
        return immagine.get();
    }
}
