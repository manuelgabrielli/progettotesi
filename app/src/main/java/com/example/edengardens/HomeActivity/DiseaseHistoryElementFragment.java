package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.DiseaseHistory.HistoryCardItem;
import com.example.edengardens.Utilities;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

public class DiseaseHistoryElementFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.scan_disease_result_fragment, container, false);
        Activity activity = getActivity();
        if(activity!=null){
            ConstraintLayout progressBarLayout = view.findViewById(R.id.progressBarScanLayout);
            progressBarLayout.setVisibility(View.GONE);

            //setto il titolo
            TextView titolo = view.findViewById(R.id.titoloScanDiseaseResult);
            titolo.setText(R.string.storico_malattie);

            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_HISTORY, Activity.MODE_PRIVATE);
            Gson gson = new Gson();
            String json = sharedPreferences.getString(Utilities.KEY_HISTORY_ELEMENT, "");
            HistoryCardItem historyCardItem = gson.fromJson(json, HistoryCardItem.class);

            //setto l'immagine
            ImageView imageView = view.findViewById(R.id.previewScanPlantImageView);
            Picasso.get()
                    .load(Utilities.IMAGE_PREFIX + historyCardItem.getImmaginePianta())
                    .resize(400, 400)
                    .onlyScaleDown()
                    .centerCrop()
                    .into(imageView);

            LinearLayout resultLayout = view.findViewById(R.id.resultViewLayout);
            resultLayout.setVisibility(View.VISIBLE);

            TextView esito = view.findViewById(R.id.esitoTextView);
            ImageView esitoImage = view.findViewById(R.id.esitoImageView);

            if(historyCardItem.isValutata() && historyCardItem.isCorretta()){
                esito.setText(R.string.esito_confermato);
            } else if(historyCardItem.isValutata() && !historyCardItem.isCorretta()){
                esito.setText(R.string.esito_modificato);
                Picasso.get().load(R.drawable.icona_attenzione).into(esitoImage);
            } else if(!historyCardItem.isValutata()){
                esito.setText(R.string.esito_da_confermare);
                Picasso.get().load(R.drawable.punto_interrogativo_icona).into(esitoImage);
            }

            TextView malattia = view.findViewById(R.id.malattiaVisualTextView);

            if(historyCardItem.getMalattia().equals(Utilities.MALATTIA_NON_DISPONIBILE)){
                TextView firstLine = view.findViewById(R.id.firstLineScanResultTextView);
                TextView secondLine = view.findViewById(R.id.secondLineScanResultTextView);
                malattia.setText(R.string.malattia_non_rilevabile);
                secondLine.setVisibility(View.GONE);
                firstLine.setVisibility(View.GONE);
            } else if(historyCardItem.getMalattia().contains(Utilities.MODEL_LABEL_HEALTHY)){
                TextView piantaSana = view.findViewById(R.id.secondLineScanResultTextView);
                piantaSana.setText(R.string.è_sana);
                malattia.setVisibility(View.GONE);
            } else {
                malattia.setText(historyCardItem.getDisease());
            }
        }
        return view;
    }
}
