package com.example.edengardens.RecyclerView.Answer;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edengardens.R;
import com.example.edengardens.Utilities;


import java.util.ArrayList;
import java.util.List;

public class AnswerCardAdapter extends RecyclerView.Adapter<AnswerCardAdapter.CardViewHolder> {

    private static final String LOG = "CardAdapter-LAB";
    //list that can be filtered
    private List<AnswerCardItem> cardItemList;
    //list that contains ALL the element added by the user
    private List <AnswerCardItem> cardItemListFull;
    private AnswerCardAdapter.OnItemListener listener;

    public AnswerCardAdapter(List<AnswerCardItem> cardItemList, AnswerCardAdapter.OnItemListener listener){
        this.cardItemList=new ArrayList<>(cardItemList);
        this.cardItemListFull=new ArrayList<>(cardItemList);
        this.listener=listener;
    }


    @NonNull
    @Override
    public AnswerCardAdapter.CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.preview_answer_layout, parent, false);
        return new AnswerCardAdapter.CardViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull AnswerCardAdapter.CardViewHolder holder, int position) {
        AnswerCardItem currentcardItemAnswer =cardItemList.get(position);
        // da completare
        holder.risposta.setText(currentcardItemAnswer.getRisposta());
        holder.date.setText(Utilities.changeDateFormat(currentcardItemAnswer.getData()));
        holder.autore.setText(currentcardItemAnswer.getAutore());
        holder.ratingBar.setRating(currentcardItemAnswer.getRaking());


    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }

    static class CardViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {


        TextView risposta;
        TextView date;
        RatingBar ratingBar;
        TextView autore;


        AnswerCardAdapter.OnItemListener itemListener;

        CardViewHolder(@NonNull View itemView, AnswerCardAdapter.OnItemListener lister) {
            super(itemView);
            risposta=itemView.findViewById(R.id.idRispostaPreview);
            autore=itemView.findViewById(R.id.idAutoreDomandaPreview);
            date=itemView.findViewById(R.id.idDataRisposta);
            ratingBar=itemView.findViewById(R.id.ratingBarIndicator);
            itemListener = lister;


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemListener{
        void onItemClick(int position);
    }


    public void updateList() {
        Log.d(LOG, "updateList()");
        cardItemList.clear();
        cardItemList.addAll(AnswerCardItemSINGLETON.getInstance().getListRisposta());
        notifyDataSetChanged();
    }
}
