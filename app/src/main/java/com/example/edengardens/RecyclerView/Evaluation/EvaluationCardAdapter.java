package com.example.edengardens.RecyclerView.Evaluation;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class EvaluationCardAdapter extends RecyclerView.Adapter<EvaluationCardAdapter.CardViewHolder> implements Filterable {
    private List<EvaluationCardItem> cardItemList;
    private List<EvaluationCardItem> cardItemListFull;

    private OnItemListener listener;

    public EvaluationCardAdapter(List<EvaluationCardItem> cardItemList, OnItemListener listener) {
        this.cardItemList = new ArrayList<>(cardItemList);
        this.cardItemListFull = new ArrayList<>(cardItemList);
        this.listener = listener;
    }

    public void updateList() {
        cardItemList.clear();
        cardItemList.addAll(EvaluationCardItemSINGLETON.getInstance().getValutazioneList());
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.evaluate_layout, parent, false);
        return new CardViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        EvaluationCardItem currentCard = cardItemList.get(position);
        holder.malattia.setText(currentCard.getMalattia());
        holder.percentuale.setText(currentCard.getPercentuale());
        holder.pianta.setText(currentCard.getPianta());

        Picasso.get()
                .load(Utilities.IMAGE_PREFIX + currentCard.getImmagine())
                .resize(100, 100)
                .centerCrop()
                .into(holder.immaginePianta);
    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }

    @Override
    public Filter getFilter() {
        return cardFilter;
    }

    static class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView malattia;
        ImageView immaginePianta;
        OnItemListener itemListener;
        TextView pianta;
        TextView percentuale;


        public CardViewHolder(@NonNull View itemView, OnItemListener lister) {
            super(itemView);
            malattia = itemView.findViewById(R.id.idMalattiaPreviewCorrezione);
            immaginePianta = itemView.findViewById(R.id.previewPiantaCorrezioneImage);
            pianta = itemView.findViewById(R.id.idPiantaPreviewCorrezione);
            percentuale = itemView.findViewById(R.id.idPercentualePreviewCorrezione);

            itemListener = lister;

            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            itemListener.onItemClick(getAdapterPosition());
        }
    }

    private Filter cardFilter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<EvaluationCardItem> filteredList = new ArrayList<>();

            //if you have no constraint --> return the full list
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(cardItemListFull);
            } else {
                //else apply the filter and return a filtered list
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (EvaluationCardItem item : cardItemListFull) {
                    if (item.getMalattia().toLowerCase().contains(filterPattern) || item.getPianta().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            cardItemList.clear();

            List<?> result = (List<?>) results.values;
            for (Object object : result) {
                if (object instanceof EvaluationCardItem) {
                    cardItemList.add((EvaluationCardItem) object);
                }
            }

            EvaluationCardItemSINGLETON.getInstance().setList(cardItemList);
            //warn the adapter that the dare are changed after the filtering
            notifyDataSetChanged();
        }
    };

    public interface OnItemListener{
        void onItemClick(int position);
    }
}
