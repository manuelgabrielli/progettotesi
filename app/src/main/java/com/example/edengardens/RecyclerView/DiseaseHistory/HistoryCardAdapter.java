package com.example.edengardens.RecyclerView.DiseaseHistory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class HistoryCardAdapter extends RecyclerView.Adapter<HistoryCardAdapter.CardViewHolder>{
    private List<HistoryCardItem> cardItemList;

    private OnItemListener listener;

    public HistoryCardAdapter(List<HistoryCardItem> cardItemList, OnItemListener listener) {
        this.cardItemList = new ArrayList<>(cardItemList);
        this.listener = listener;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.disease_layout, parent, false);
        return new CardViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryCardAdapter.CardViewHolder holder, int position) {
        HistoryCardItem currentCard = cardItemList.get(position);
        holder.malattia.setText(currentCard.getDisease());
        holder.pianta.setText(currentCard.getPianta());
        Picasso.get()
                .load(Utilities.IMAGE_PREFIX + currentCard.getImmaginePianta())
                .resize(100, 100)
                .centerCrop()
                .into(holder.immaginePianta);

        if(currentCard.isValutata()){ //faccio vedere il tick verde se è stata valutata
            holder.immagineValutata.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }

    public void updateList() {
        cardItemList.clear();
        cardItemList.addAll(HistoryCardItemSINGLETON.getInstance().getMalattieList());
        notifyDataSetChanged();
    }

    static class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView malattia;
        ImageView immaginePianta;
        OnItemListener itemListener;
        ImageView immagineValutata;
        TextView pianta;

        public CardViewHolder(@NonNull View itemView, OnItemListener lister) {
            super(itemView);
            malattia = itemView.findViewById(R.id.idNomeMalattia);
            immaginePianta = itemView.findViewById(R.id.previewPiantaImage);
            immagineValutata = itemView.findViewById(R.id.idMalattiaValutataImageView);
            pianta = itemView.findViewById(R.id.idNomePiantaMalattia);

            itemListener = lister;

            ImageButton button = itemView.findViewById(R.id.cancelDiseaseImageButton);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //handler pulsante per cancellare la voce nello storico
                    itemListener.onButtonDeleteDiseaseClick(getAdapterPosition(), v);
                }
            });

            itemView.setOnClickListener(this);
        }
            @Override
        public void onClick(View view) {
                itemListener.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemListener{
        void onItemClick(int position);
        void onButtonDeleteDiseaseClick(int position, View v);
    }
}
