package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.hadisatrio.optional.Optional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

import org.tensorflow.lite.support.common.FileUtil;
import org.tensorflow.lite.support.common.TensorProcessor;
import org.tensorflow.lite.support.common.ops.NormalizeOp;
import org.tensorflow.lite.support.image.ImageProcessor;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.image.ops.ResizeOp;
import org.tensorflow.lite.support.image.ops.ResizeWithCropOrPadOp;
import org.tensorflow.lite.support.label.TensorLabel;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;


public class ScanDiseaseResultFragment extends Fragment {
    private static final int QUALITY = 100;
    private static final String TAG = "SCAN_DISEASE_TAG";
    private LinearLayout resultLayout;
    private LinearLayout piantaSanaLayout;
    private ConstraintLayout errorLayout;
    private ConstraintLayout progressBarLayout;
    private ConstraintLayout progressBarContributeLayout;
    private Interpreter tflite;
    private boolean modelloCaricato = true;
    private List<String> modelLabels;
    private Bitmap imageBitmap;
    private Activity activity;
    private TextView nomeMalattia;
    private TextView percentualeRisultato;
    private TextView percentualeRisultatoSana;
    private String piantaSanaNome;
    private String piantaCategoria;
    private Button contribuisciResult;
    private Button contribuisciSana;
    private Connection connection;
    private AlertDialog problemAlert;
    private String imageName;

    /** Labels corresponding to the output of the vision model. */
    private List<String> labels;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.scan_disease_result_fragment, container, false);

        activity = getActivity();
        if(activity != null){
            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);
            resultLayout = view.findViewById(R.id.resultScanLinearLayout);
            piantaSanaLayout = view.findViewById(R.id.piantaSanaScanLinearLayout);
            errorLayout = view.findViewById(R.id.errorScanConstraintLayout);
            progressBarLayout = view.findViewById(R.id.progressBarScanLayout);
            progressBarContributeLayout = view.findViewById(R.id.progressBarContributeLayout);
            nomeMalattia = view.findViewById(R.id.malattiaScanTextView);
            percentualeRisultato = view.findViewById(R.id.percentualeRisultatoTextView);
            percentualeRisultatoSana = view.findViewById(R.id.percentualeRisultatoSanaTextView);

            //costruisco il nome della foto
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            imageName = "JPEG_" + timeStamp + ".jpg";

            //recupero l'immagine e la setto nella imageView
            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_SCAN_DISEASE, Activity.MODE_PRIVATE);
            Gson gson = new Gson();
            String json;
            json = sharedPreferences.getString(Utilities.KEY_SCAN_DISEASE, "");
            imageBitmap = gson.fromJson(json, Bitmap.class);

            piantaCategoria = sharedPreferences.getString(Utilities.SCAN_PLANT_DISEASE, "");

            ImageView imageView = view.findViewById(R.id.previewScanPlantImageView);
            imageView.setImageBitmap(imageBitmap);

            contribuisciResult = view.findViewById(R.id.idContribuisciResultButton);
            contribuisciSana = view.findViewById(R.id.idContribuisciSanaButton);


             problemAlert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.problema)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            //carico il modello
            try{
                tflite = new Interpreter(loadModelFile(activity));

                //controllo lingua e caricamento del giusto file con le labels, all'aggiunta di nuove lingue basta inserire altri case
                //da togliere commento quando verrà inserita la lingua inglese
                String language = Locale.getDefault().getLanguage();
                /*switch (language){
                    case "it": labels = FileUtil.loadLabels(activity, "labelsIta.txt");
                                piantaSanaNome = getString(R.string.sana);
                                break;
                    default: labels = FileUtil.loadLabels(activity, "labelsEng.txt"); //lingua inglese
                              piantaSanaNome = getString(R.string.sana);
                }*/

                //solo con lingua italiana
                labels = FileUtil.loadLabels(activity, "labelsIta.txt");
                piantaSanaNome = getString(R.string.sana);

            } catch (IOException e){
                modelloCaricato = false;
                problemAlert.show();
            }
        }
        return view;
    }

    private ByteBuffer loadModelFile(Activity activity) throws IOException {
        AssetFileDescriptor fileDescriptor = activity.getAssets().openFd("mobilenetv2.tflite");
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //leggo i nomi del modello generali
        try {
            modelLabels = FileUtil.loadLabels(activity, "labelsModel.txt");
        } catch (IOException e) {
            problemAlert.show();
            modelloCaricato = false;
        }

        if(modelloCaricato){
            new Classifier(activity, tflite, piantaCategoria).start();
        }
    }

    //inner class per eseguire il modello
    private class Classifier extends Thread{

        private static final float IMAGE_MEAN = 0.0f;
        private static final float IMAGE_STD = 1.0f;

        private  final Activity classifierActivity;
        private final Interpreter classifierInterpreter;

        //dimensione immagine
        private int imageSizeX;
        private int imageSizeY;

        //buffer di input
        private TensorImage inputImageBuffer;

        private final String pianta;

        public Classifier(Activity activity, Interpreter interpreter, String pianta) {
            this.classifierActivity = activity;
            this.classifierInterpreter = interpreter;
            this.pianta = pianta;
        }

        @Override
        public void run() {
            //tensorflow lite
            try {
                predictDisease();
            } catch (Exception e){
                classifierActivity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        //intercetto un errore nella predizione e mostro la view di errore
                        errorLayout.setVisibility(View.VISIBLE);
                        progressBarLayout.setVisibility(View.GONE);
                    }
                });
            }
        }

        private void predictDisease() {
            int imageTensorIndex = 0;
            int[] imageShape = classifierInterpreter.getInputTensor(imageTensorIndex).shape();
            imageSizeY = imageShape[1];
            imageSizeX = imageShape[2];
            DataType imageDataType = classifierInterpreter.getInputTensor(imageTensorIndex).dataType();
            int probabilityTensorIndex = 0;
            int[] probabilityShape =
                    classifierInterpreter.getOutputTensor(probabilityTensorIndex).shape();
            DataType probabilityDataType = classifierInterpreter.getOutputTensor(probabilityTensorIndex).dataType();

            // creo l'input tensor
            inputImageBuffer = loadImage(imageBitmap, imageDataType);

            //buffer di output
            TensorBuffer outputProbabilityBuffer = TensorBuffer.createFixedSize(probabilityShape, probabilityDataType);

            // buffer di output per la probabilità
            TensorProcessor probabilityProcessor = new TensorProcessor.Builder()
                                                                      .add(new NormalizeOp(IMAGE_MEAN, IMAGE_STD))
                                                                      .build();

            classifierInterpreter.run(inputImageBuffer.getBuffer(), outputProbabilityBuffer.getBuffer().rewind());

            Map<String, Float> labeledProbability =
                    new TensorLabel(labels, probabilityProcessor.process(outputProbabilityBuffer))
                            .getMapWithFloatValue();

            final ScanResult risultatoScan = getTopProbability(labeledProbability);

            //RISULTATO

            //modifico la view nel thread principale
            classifierActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    //gestione pulsanti per caricare l'immagine
                    contribuisciResult.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (connection.isConnected()) {
                                updateFoto(risultatoScan.getModelDisease(), true, Optional.<LinearLayout>absent(), risultatoScan.getConfidence());
                            } else{
                                Log.e(TAG, "onClick: no connection");
                                connection.showSnackbar();
                            }
                        }
                    });

                    contribuisciSana.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (connection.isConnected()) {
                                updateFoto(risultatoScan.getModelDisease(), true, Optional.<LinearLayout>absent(), risultatoScan.getConfidence());
                            } else{
                                Log.e(TAG, "onClick: no connection");
                                connection.showSnackbar();
                            }
                        }
                    });

                    //se la pianta è sana faccio vedere la view apposita
                    if(risultatoScan.getDisease().contains(piantaSanaNome)){
                        String percentuale = "("+ getString(R.string.percentuale_risultato, risultatoScan.getConfidence()*100) +"%)";
                        percentualeRisultatoSana.setText(percentuale);
                        if(risultatoScan.getConfidence() > 0.5){
                            contribuisciSana.setVisibility(View.VISIBLE);
                            piantaSanaLayout.setVisibility(View.VISIBLE);
                            progressBarLayout.setVisibility(View.GONE);
                        } else{
                            updateFoto(risultatoScan.getModelDisease(), false, Optional.of(piantaSanaLayout), risultatoScan.getConfidence());
                        }

                    } else {
                        //setto il nome della malattia e la percentuale
                        nomeMalattia.setText(risultatoScan.getDisease());
                        String percentuale = "("+getString(R.string.percentuale_risultato, risultatoScan.getConfidence()*100) +"%)";
                        percentualeRisultato.setText(percentuale);
                        if(risultatoScan.getConfidence() > 0.5){
                            contribuisciResult.setVisibility(View.VISIBLE);
                            resultLayout.setVisibility(View.VISIBLE);
                            progressBarLayout.setVisibility(View.GONE);
                        } else{
                            updateFoto(risultatoScan.getModelDisease(), false, Optional.of(resultLayout), risultatoScan.getConfidence());
                        }
                    }
                }
            });
        }

        private TensorImage loadImage(final Bitmap bitmap, DataType imageDataType) {

            inputImageBuffer = new TensorImage(imageDataType);
            inputImageBuffer.load(bitmap);

            //converto l'immagine in una leggibile dall'interprete
            int cropSize = Math.min(bitmap.getWidth(), bitmap.getHeight());

            ImageProcessor imageProcessor =
                    new ImageProcessor.Builder()
                            .add(new ResizeWithCropOrPadOp(cropSize, cropSize))
                            .add(new ResizeOp(imageSizeX, imageSizeY, ResizeOp.ResizeMethod.NEAREST_NEIGHBOR))
                            .add(new NormalizeOp(IMAGE_MEAN, IMAGE_STD))
                            .build();
            return imageProcessor.process(inputImageBuffer);
        }

        //restituisce il risultato migliore
        private ScanResult getTopProbability(Map<String, Float> labelProb) {
            // trova la classificazione con il punteggio più alto
            PriorityQueue<ScanResult> pq =
                    new PriorityQueue<>(
                            1,
                            new Comparator<ScanResult>() {
                                @Override
                                public int compare(ScanResult lhs, ScanResult rhs) {
                                    return Float.compare(rhs.getConfidence(), lhs.getConfidence());
                                }
                            });

            int index = 0;
            if(pianta.equals(getString(R.string.tuttePiante))){

                for (Map.Entry<String, Float> entry : labelProb.entrySet()) {
                    //se l'utente ha inserito l'opzione tutte le piante
                    pq.add(new ScanResult(entry.getKey(), entry.getValue(), modelLabels.get(index)));
                    index++;
                }
            } else {
                for (Map.Entry<String, Float> entry : labelProb.entrySet()) {
                    if (entry.getKey().contains(pianta)) { //filtro solo le malattie della pianta indicata dall'utente
                        pq.add(new ScanResult(entry.getKey(), entry.getValue(), modelLabels.get(index)));
                    }
                    index++;
                }
            }



            return pq.poll();
        }
    }

    //salva la foto sul server
    private void updateFoto(final String nomeMalattia, final boolean corretta, final Optional<LinearLayout> layoutOptional, final float percentuale) {
        if(corretta) {
            progressBarContributeLayout.setVisibility(View.VISIBLE);
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        imageBitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY, byteArrayOutputStream);

        //rappresentazione in stringa dell'immagine
        final String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

        String url = "https://ftp.edengardens.altervista.org/savePicture.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            //qualcosa è andato storto
                            if(!jsonObject.getBoolean("result")) {
                                //cambio il messaggio di errore
                                problemAlert.setMessage(getResources().getString(R.string.problema_caricamento));
                                problemAlert.show();
                            } else{
                                createAnalisiPianta(nomeMalattia, corretta, layoutOptional, percentuale);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("nome",imageName);
                params.put("immagine", encodedImage);

                return params;
            }
        };
        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }

    //salvo un entry nella taella
    private void createAnalisiPianta(final String nomeMalattia, final boolean corretta, final Optional<LinearLayout> layoutOptional, final float percentuale) {
        String url;
        if(corretta) {
             url = "https://ftp.edengardens.altervista.org/analisiPiante.php?correct=true";
        } else{
             url = "https://ftp.edengardens.altervista.org/analisiPiante.php?";
        }
        final String username = activity.getSharedPreferences(Utilities.KEY_LOGIN, Context.MODE_PRIVATE).getString(Utilities.KEY_USERNAME, "");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            //qualcosa è andato storto
                            if(!jsonObject.getBoolean("result")) {
                                //cambio il messaggio di errore
                                problemAlert.setMessage(getResources().getString(R.string.problema_caricamento));
                                problemAlert.show();
                                progressBarLayout.setVisibility(View.GONE);
                            } else{
                                 if(corretta){ //handle nel caso sia derivato da un upload di una foto corretta
                                     progressBarContributeLayout.setVisibility(View.GONE);
                                     AlertDialog confirmAlert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                             .setTitle(R.string.congratulazioni)
                                             .setMessage(R.string.avvenuto_caricamento)
                                             .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                 public void onClick(DialogInterface dialog, int id) {
                                                 }
                                             }).create();
                                     confirmAlert.show();
                                     //rendo non utilizzabili i pulsanti
                                     contribuisciResult.setEnabled(false);
                                     contribuisciSana.setEnabled(false);
                                 }
                                 else{
                                     if(layoutOptional.isPresent()){
                                         layoutOptional.get().setVisibility(View.VISIBLE);
                                     }
                                     progressBarLayout.setVisibility(View.GONE);
                                 }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            problemAlert.setMessage(getResources().getString(R.string.problema_caricamento));
                            problemAlert.show();
                            progressBarLayout.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("nomeImmagine",imageName);
                params.put("nomeMalattia",nomeMalattia);
                params.put("email",username);
                params.put("percentuale", ""+percentuale);

                return params;
            }
        };
        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }

    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();
        connection.close();
    }

}
