package com.example.edengardens.EvaluateActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.Connection;
import com.example.edengardens.MainActivity.MainActivity;
import com.example.edengardens.R;
import com.example.edengardens.RecyclerView.Evaluation.EvaluationCardAdapter;
import com.example.edengardens.RecyclerView.Evaluation.EvaluationCardItem;
import com.example.edengardens.RecyclerView.Evaluation.EvaluationCardItemSINGLETON;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class EvaluateFragment extends Fragment implements EvaluationCardAdapter.OnItemListener{
    private static final String TAG = "EVALUATE_REQUEST";
    private FragmentActivity activity;
    private Connection connection;
    private ConstraintLayout progressBar;
    private ConstraintLayout noValutazioni;
    private AlertDialog alert;
    private View view;
    private boolean primaVolta = true;
    private EvaluationCardAdapter adapter;
    private SearchView searchView;
    private boolean firstTime = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.evaluate_fragment, container, false);

        activity = getActivity();
        if(activity!=null){
            connection = new Connection(getContext(), activity.findViewById(R.id.evaluateLayoutActivity), activity, TAG);

            TextView logout = view.findViewById(R.id.logoutAdmin);
            progressBar = view.findViewById(R.id.progressBarValutazioneLayout);
            noValutazioni = view.findViewById(R.id.noValutazioniLayout);

            connection.setNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    connection.setNetworkConnected(true);
                    connection.dismissSnackbar();
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() { //questo metodo serve per far girare il codice sul thread principale, perchè solo li posso cambaire la view
                           if(primaVolta){
                               primaVolta = false;
                               requestEvaluations();
                           }
                        }
                    });

                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    connection.setNetworkConnected(false);
                    connection.showSnackbar();
                }

            });

            searchView = view.findViewById(R.id.idSearchEvaluate);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    return true;
                }
            });

            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.apply();

                    Intent intent = new Intent(activity, MainActivity.class);
                    startActivity(intent);
                    activity.finish();
                }
            });

            alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                    .setTitle(R.string.errore)
                    .setMessage(R.string.problema)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).create();

            Button ricarica = view.findViewById(R.id.ricaricaButton);
            ricarica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (connection.isConnected()) {
                        requestEvaluations();
                    }  else {
                        Log.e(TAG, "onClick: no connection");
                        connection.showSnackbar();
                    }
                }
            });

        }

        return view;
    }

    private void setRecyclerView(){
        // Set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.evaluateRecyclerView);

        adapter = new EvaluationCardAdapter(EvaluationCardItemSINGLETON.getInstance().getValutazioneList(), this);
        recyclerView.setAdapter(adapter);

        if(EvaluationCardItemSINGLETON.getInstance().getValutazioneList().isEmpty()){
            //faccio vedere il layout della scritta
            noValutazioni.setVisibility(View.VISIBLE);
        } else {
            noValutazioni.setVisibility(View.GONE);
        }

        if(firstTime) {
            //setto la ricerca che c'era gia in precedenza
            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_VALUTAZIONESELEZIONATA, MODE_PRIVATE);
            String ricerca = sharedPreferences.getString(Utilities.KEY_RICERCA_EVALUATE,"");
            searchView.setQuery(ricerca, true);
            firstTime = false;
        }
    }

    private void requestEvaluations() {
        String url = "https://ftp.edengardens.altervista.org/analisiPiante.php";

        progressBar.setVisibility(View.VISIBLE);

        EvaluationCardItemSINGLETON.getInstance().EmptyList();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String immagine = obj.getString("immagine_pianta");
                                String nomeMalattia = obj.getString("nome_malattia");
                                String email = obj.getString("email_utente");
                                boolean eliminata = obj.getInt("eliminata") != 0;
                                String percentuale = obj.getString("percentuale");

                                EvaluationCardItem cardItem = new EvaluationCardItem(nomeMalattia,
                                        immagine,email,percentuale,eliminata);
                                EvaluationCardItemSINGLETON.getInstance().addCardItemValutazione(cardItem);
                            }
                            setRecyclerView();
                            progressBar.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                            alert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("admin","true");
                return params;
            }

        };
        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);
        connection.addRequest(stringRequest);
    }

    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }

    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo
        connection.close();
    }

    @Override
    public void onItemClick(int position) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_VALUTAZIONESELEZIONATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
        Gson gson = new Gson();
        String json = gson.toJson(EvaluationCardItemSINGLETON.getInstance().getCardValutazioneItem(position));
        editor.putString(Utilities.KEY_VALUTAZIONESELEZIONATA,json);
        editor.putString(Utilities.KEY_RICERCA_EVALUATE, searchView.getQuery().toString());
        editor.apply(); //NECESSARIO

        Utilities.insertFragment(activity, new CorrectFragment(), "CorrectFragment", R.id.evaluateLayoutActivity, true);
    }
}
