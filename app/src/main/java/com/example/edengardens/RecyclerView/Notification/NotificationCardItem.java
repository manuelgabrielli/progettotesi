package com.example.edengardens.RecyclerView.Notification;

import com.hadisatrio.optional.Optional;

public class NotificationCardItem {
    private final String testo;
    private final String data;
    private final String userMittente;
    private Optional<String> immagine = Optional.absent();
    private Optional<String> idRisposta = Optional.absent();
    private Optional<String> idDomanda = Optional.absent();
    private Optional<String> idAnalisiPianta = Optional.absent();

    public NotificationCardItem(String userMittente, String testo, String data, String immagine, String idDomanda, String idRisposta, String idAnalisiPianta) {
        this.userMittente = userMittente;
        this.testo = testo;
        this.data = data;
        if(!immagine.equals("null"))
            this.immagine = Optional.of(immagine);
        if(!idDomanda.equals("null"))
            this.idDomanda = Optional.of(idDomanda);
        if(!idRisposta.equals("null"))
            this.idRisposta = Optional.of(idRisposta);
        if(!idAnalisiPianta.equals("null"))
            this.idAnalisiPianta = Optional.of(idAnalisiPianta);
    }

    public NotificationCardItem(String userMittente, String testo, String data, String idDomanda, String idRisposta, String idAnalisiPianta) {
        this.userMittente = userMittente;
        this.testo = testo;
        this.data = data;
        if(!idDomanda.equals("null"))
            this.idDomanda = Optional.of(idDomanda);
        if(!idRisposta.equals("null"))
            this.idRisposta = Optional.of(idRisposta);
        if(!idAnalisiPianta.equals("null"))
            this.idAnalisiPianta = Optional.of(idAnalisiPianta);
    }

    public String getTesto() {
        return testo;
    }

    public String getData() {
        return data;
    }

    public String getUserMittente() {
        return userMittente;
    }

    public boolean isImagePresent(){
        return immagine.isPresent();
    }

    public String getImmagine() {
        return immagine.get();
    }

    public boolean isDomandaPresent(){
        return idDomanda.isPresent();
    }

    public int getIdDomanda() {
        return Integer.parseInt(idDomanda.get());
    }

    public boolean isRispostaPresent(){
        return idRisposta.isPresent();
    }

    public int getIdRisposta() {
        return Integer.parseInt(idRisposta.get());
    }

    public boolean isAnalisiPiantaPresent(){
        return idAnalisiPianta.isPresent();
    }

    public String getIdAnalisiPianta() {
        return idAnalisiPianta.get();
    }
}
