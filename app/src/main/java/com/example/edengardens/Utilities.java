package com.example.edengardens;

import android.app.Activity;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.gson.internal.$Gson$Preconditions;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

public class Utilities {

    public static final String KEY_LOGIN = BuildConfig.APPLICATION_ID + ".SHARED_PREF";//per renderla univoca
    public static final String KEY_USERNAME = "USERNAME";
    public static final String KEY_CITTA="CITTA";
    public static final String KEY_NOME="NOME";
    public static final String KEY_COGNOME="COGNOME";
    public static final String KEY_DOMANDASELEZIONATA="DOMANDA_SELEZIONATA";
    public static final String KEY_DOMANDASELEZIONATA_HOME="DOMANDA_SELEZIONATA_HOME";
    public static final String KEY_DOMANDASELEZIONATA_FOLLOW="DOMANDA_SELEZIONATA_FOLLOW";
    public static final String KEY_DOMANDASELEZIONATA_SEARCH="DOMANDA_SELEZIONATA_SEARCH";
    public static final String KEY_RISPOSTA1SELEZIONATA="RISPOSTA1_SELEZIONATA";
    public static final String KEY_CATEGORIA = "CATEGORIA_SELEZIONATA";
    public static final String KEY_PIANTA_SELEZIONATA = "PIANTA_SELEZIONATA";
    public static final String IMAGE_PREFIX = "https://edengardens.altervista.org/";
    public static final String KEY_IMMAGINE_PROFILO = "IMMAGINE_PROFILO";
    public static final String KEY_SCAN_DISEASE = "SCAN_DISEASE";
    public static final String KEY_HISTORY = "DISEASE_HISTORY";
    public static final String KEY_HISTORY_ELEMENT = "DISEASE_HISTORY_ELEMENT";
    public static final String SCAN_PLANT_DISEASE = "SCAN_PLANT_DISEASE";
    public static final String KEY_OTHER_USER_PROFILE = "OTHER_USER_PROFILE";
    public static final String KEY_ADMIN = "LOGIN_ADMIN";
    public static final String KEY_VALUTAZIONESELEZIONATA = "VALUTAZIONE_SELEZIONATA";
    public static final String KEY_RICERCA_EVALUATE = "RICERCA_EVALUATE";
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String CATEGORIA_DOMANDA = "CATEGORIA_DOMANDA";
    public static final String NULL = "null";
    public static final int REQUEST_IMAGE_CAPTURE = 2;
    public static final int ACTIVITY_ADD_QUESTION = 1;
    public static final int PICK_FROM_GALLERY = 3;
    private static final String key = "qLaKUOsHYTCwsMhjkBOWTYQQ6JegvmrM";
    private static final String salt = "<EBR~[E30=:d?Hs&g7Z,9;knFb97N{{Q]`[n1rO=hH3J*Q.W;,VZv`,M^%FHbeA0";
    public static final String LABELS_SPLITTER = "___";
    public static final String MODEL_LABEL_HEALTHY = "healthy";
    public static final String MALATTIA_NON_DISPONIBILE = "None";

    static public void insertFragment(FragmentActivity activity, Fragment fragment, String tag, int idFragmentContainer, boolean isFragment) {
        if(isFragment) {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(idFragmentContainer, fragment, tag)
                    .addToBackStack(null)
                    .commit();
        } else{
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .add(idFragmentContainer, fragment, tag)
                    .commit();
        }
    }

    public static Bitmap getImageBitmap(Activity activity, Uri currentPhotoUri){
        ContentResolver resolver = activity.getApplicationContext()
                .getContentResolver();
        try {
            InputStream stream = resolver.openInputStream(currentPhotoUri);
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            Objects.requireNonNull(stream).close();
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String changeDateFormat(String oldDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(oldDate);

        } catch (ParseException e) {
            e.printStackTrace();
            return oldDate;
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("dd/MM/yy, HH:mm");
        if(sourceDate != null){
            return targetFormat.format(sourceDate);
        } else{
            return oldDate;
        }
    }

    public static String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+2"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getKey(){
        return key;
    }

    public static String getSalt(){
        return salt;
    }
}
