package com.example.edengardens.RecyclerView.Category;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edengardens.R;

import java.util.ArrayList;
import java.util.List;


public class CategoryCardAdapter extends RecyclerView.Adapter<CategoryCardAdapter.CardViewHolderCategory> implements Filterable {

    private static final String LOG = "CardAdapter-LAB";
    //list that can be filtered
    private List <CategoryCardItem> cardItemList;
    //list that contains ALL the element added by the user
    private List <CategoryCardItem> cardItemListFull;

    //listener attached to the onclick event for the item in yhe RecyclerView
    private OnItemListener listener;

    public CategoryCardAdapter(List<CategoryCardItem> cardItemList, OnItemListener listener) {
        this.cardItemList = new ArrayList<>(cardItemList);
        this.cardItemListFull = new ArrayList<>(cardItemList);
        this.listener = listener;
    }


    @NonNull
    @Override
    public CardViewHolderCategory onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_layout, parent, false);
        return new CardViewHolderCategory(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolderCategory holder, int position) {
        CategoryCardItem currentCardItem = cardItemList.get(position);
        holder.nome.setText(currentCardItem.getNome());
    }

    @Override
    public int getItemCount() {
        return cardItemList.size();
    }


    @Override
    public Filter getFilter() {
        return cardFilter;
    }


    static class CardViewHolderCategory extends RecyclerView.ViewHolder  implements View.OnClickListener {

        TextView nome;


        OnItemListener itemListener;

        CardViewHolderCategory(@NonNull View itemView, OnItemListener lister) {
            super(itemView);
            nome=itemView.findViewById(R.id.idTitoloCategoriaPreview);

            itemListener = lister;


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.onItemClick(getAdapterPosition());
        }
    }

    private Filter cardFilter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<CategoryCardItem> filteredList = new ArrayList<>();

            //if you have no constraint --> return the full list
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(cardItemListFull);
            } else {
                //else apply the filter and return a filtered list
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (CategoryCardItem item : cardItemListFull) {
                    if (item.getNome().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            cardItemList.clear();

            List<?> result = (List<?>) results.values;
            for (Object object : result) {
                if (object instanceof CategoryCardItem) {
                    cardItemList.add((CategoryCardItem) object);
                }
            }

            CategoryCardItemSINGLETON.getInstance().setList(cardItemList);
            //warn the adapter that the dare are changed after the filtering
            notifyDataSetChanged();
        }
    };


    public interface OnItemListener{
        void onItemClick(int position);
    }

}
