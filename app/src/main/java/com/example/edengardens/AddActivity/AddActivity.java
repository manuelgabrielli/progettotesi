package com.example.edengardens.AddActivity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.edengardens.R;
import com.example.edengardens.Utilities;

public class AddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        Utilities.insertFragment(this, new AddFragment(), "addFragment", R.id.fragment_container_add, false);
    }
}
