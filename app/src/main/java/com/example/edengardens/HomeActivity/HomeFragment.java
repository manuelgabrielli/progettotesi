package com.example.edengardens.HomeActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edengardens.AddActivity.AddActivity;
import com.example.edengardens.RecyclerView.Question.QuestionCardAdapter;
import com.example.edengardens.RecyclerView.Question.QuestionCardItemSINGLETON;
import com.example.edengardens.RecyclerView.Question.QuestionCardItem;
import com.example.edengardens.Connection;
import com.example.edengardens.R;
import com.example.edengardens.Utilities;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment implements QuestionCardAdapter.OnItemListener {

    private static final String LOG = "Home-Fragment_LAB";

    private QuestionCardAdapter adapter;
    private RecyclerView recyclerView;

    private static final String TAG = "HOME_REQUEST";
    private Connection connection;

    private ConstraintLayout progressBarLayout;

    private View view;

    private ImageView notificationPoint;
    private ConstraintLayout noDomandaLayout;
    private Activity activity;

    private String email;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.home_fragment, container, false);

        activity = getActivity();
        if(activity != null) {
            connection = new Connection(getContext(), activity.findViewById(R.id.fragment_container_home), activity, TAG);

            SearchView searchView = view.findViewById(R.id.idSearchHome);

            noDomandaLayout = view.findViewById(R.id.noDomandeLayout);

            SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_LOGIN, MODE_PRIVATE);
            email = sharedPreferences.getString(Utilities.KEY_USERNAME,"");

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    return true;
                }
            });

            notificationPoint = view.findViewById(R.id.notificationPointImageView);

            //riscrivo la netwrokcallback della connection in modo che ogni volta che sente internet ricarica la pagina
            connection.setNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    connection.setNetworkConnected(true);
                    connection.dismissSnackbar();
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() { //questo metodo serve per far girare il codice sul thread principale, perchè solo li posso cambaire la view
                            if (QuestionCardItemSINGLETON.getInstance().getListDomanda().isEmpty()) {
                                createRequest(view);
                            } else {
                                try { //se la recycler view non ha già un adapter gliene assegno uno
                                    recyclerView.getAdapter();
                                } catch (Exception e) {
                                    setRecyclerView(view);
                                }
                            }

                            askNotification();
                        }
                    });

                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    connection.setNetworkConnected(false);
                    connection.showSnackbar();
                }

            });

            final FragmentActivity fragmentActivity = getActivity();
                FloatingActionButton floatingActionButton = view.findViewById(R.id.fab_add_nuova_domanda);
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fragmentActivity.startActivityForResult(new Intent(fragmentActivity, AddActivity.class),
                                Utilities.ACTIVITY_ADD_QUESTION);
                    }
                });



            progressBarLayout = view.findViewById(R.id.progressBarHomeLayout);

            ImageButton notificationButton = view.findViewById(R.id.notificationButton);
            notificationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity act = (HomeActivity)activity;
                    act.addIdInBackList(act.getLastItemInBackList());

                    notificationPoint.setVisibility(View.GONE);

                    //cambiare fragment in home activity, manca da fare il fragment
                    Utilities.insertFragment((FragmentActivity) activity, new NotificationFragment(), "NotificationFragment", R.id.fragment_container_home, true);

                }
            });

            connection.showSnackbar(); //faccio vidualizzare di default la snackbar che non c'è connessione
        }
        return view;
    }

    private void askNotification() {

        String url = "https://ftp.edengardens.altervista.org/home.php?notifiche="+email;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject obj = jsonArray.getJSONObject(0);

                            if(obj.getInt("notifiche") != 0){
                                notificationPoint.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
        };

        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);

        connection.addRequest(stringRequest);
    }


    private void setRecyclerView(View view){
        // Set up the RecyclerView
        recyclerView = view.findViewById(R.id.idRecyclerHome);

        adapter = new QuestionCardAdapter(QuestionCardItemSINGLETON.getInstance().getListDomanda(), this, this);
        recyclerView.setAdapter(adapter);

        if(QuestionCardItemSINGLETON.getInstance().getListDomanda().isEmpty()){
            //faccio vedere il layout della scritta

            TextView noDomandeText = view.findViewById(R.id.noDomandeText);

            noDomandeText.setText(R.string.noDomandeCreate);

            noDomandaLayout.setVisibility(View.VISIBLE);
        } else {
            noDomandaLayout.setVisibility(View.GONE);
        }
    }


    private void createRequest(final View view){

        String url = "https://ftp.edengardens.altervista.org/home.php?email="+email;

        progressBarLayout.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0;i<jsonArray.length();i++){
                                JSONObject obj = jsonArray.getJSONObject(i);
                                int id=obj.getInt("idDomanda");
                                String titolo=obj.getString("titolo");
                                String domanda=obj.getString("testo");
                                String date=obj.getString("orario");
                                String fiore= obj.getString("nomePianta");
                                String autore=obj.getString("email");
                                String immagine=obj.getString("immagine_domanda");
                                QuestionCardItem questionCardItem;
                                if(!immagine.equals(Utilities.NULL)){
                                    questionCardItem = new QuestionCardItem(id,titolo,domanda,date,immagine,fiore,autore);
                                } else{
                                    questionCardItem = new QuestionCardItem(id,titolo,domanda,date,fiore,autore);
                                }
                                QuestionCardItemSINGLETON.getInstance().addCardItemDomanda(questionCardItem);
                            }

                            progressBarLayout.setVisibility(View.GONE);
                            setRecyclerView(view);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBarLayout.setVisibility(View.GONE);
                            AlertDialog alert = new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                                    .setTitle(R.string.errore)
                                    .setMessage(R.string.problema)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    }).create();
                            alert.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                    }
                }){
        };
        //bisogna assegnarli un tag e inserirlo nella coda
        stringRequest.setTag(TAG);
        connection.addRequest(stringRequest);
    }

    @Override
    public void onStart() {
        super.onStart();

        connection.registerNetworkCallback();
    }


    @Override
    public void onStop() {
        super.onStop();

        //se abbiamo una request la cancelliamo
        connection.close();
    }

    @Override
    public void onItemClick(int position) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(Utilities.KEY_DOMANDASELEZIONATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit(); //serve per aggiungere dati allo sP
        Gson gson = new Gson();
        //faccio il controllo se è stata eseguita una ricerca perchè nel caso devo predere
        //la domanda da un'altra lista
        String json;
        if(QuestionCardItemSINGLETON.getInstance().isQuestionListFiltered()){
           json = gson.toJson(QuestionCardItemSINGLETON.getInstance().getFilteredCardDomandaItem(position));
        } else{
            json = gson.toJson(QuestionCardItemSINGLETON.getInstance().getCardDomandaItem(position));
        }
        editor.putString(Utilities.KEY_DOMANDASELEZIONATA_HOME,json);
        editor.putString(Utilities.CATEGORIA_DOMANDA, Utilities.KEY_DOMANDASELEZIONATA_HOME);
        editor.apply(); //NECESSARIO

        HomeActivity activity = (HomeActivity)getActivity();
        if(activity != null)
            activity.addIdInBackList(activity.getLastItemInBackList());

        Utilities.insertFragment(activity, new ExpandedQuestionFragment(), "ExpandedQuestionFragment", R.id.fragment_container_home, true);
    }

    /**
     * metodo chiamato per aggiornare la lista
     */
    void updateList() {
        Log.d(LOG, "updateList()");
        noDomandaLayout.setVisibility(View.GONE);
        adapter.updateList();
    }
}